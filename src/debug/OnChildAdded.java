package debug;

import java.util.HashMap;
import java.util.Map;

import com.tomting.aquabase.client.core.Aquabase;
import com.tomting.aquabase.client.core.ChildEventListener;
import com.tomting.aquabase.client.core.DataSnapshot;
import com.tomting.aquabase.client.core.GenericTypeIndicator;
import com.tomting.aquabase.client.persistence.PersistenceManagerFactory;

public class OnChildAdded {
	
	public static Map<String, Object> getValue (String first, String last) {
		
		Map<String, Object> nestedValueMap = new HashMap<String, Object> ();
		nestedValueMap.put("first", first);	
		nestedValueMap.put("last", last);
		Map<String, Object> result = new HashMap<String, Object> ();		
		result.put("name", nestedValueMap);
		return result;
	}

	public static void main(String[] args) throws Exception {

		Aquabase.goOffline();		
		Aquabase usersRef = new Aquabase ("https://aquabase/testroot/users/");
		usersRef.child("fred").setValue(getValue ("Fred", "Flinstone"));
		usersRef.child("jack").setValue(getValue ("Jack", "Sparrow"));		
		usersRef.child("spiderman").setValue(getValue ("Peter", "Spiderman"));		

		ChildEventListener listener = new ChildEventListener() {
		    @Override
		    public void onChildAdded(DataSnapshot snapshot, String previousChildName) {
		        String userName = snapshot.getName();
		        GenericTypeIndicator<Map<String, Object>> t = new GenericTypeIndicator<Map<String, Object>>() {};
		        @SuppressWarnings("unused")
				Map<String, Object> userData = snapshot.getValue(t);
		        System.out.println("User " + userName + " has entered the chat after " + previousChildName + ".");
		    }

		    @Override
		    public void onChildChanged(DataSnapshot snapshot, String previousChildName) {
		        String userName = snapshot.getName();		        
		        String firstName = (String)snapshot.child("name/first").getValue();
		        String lastName = (String)snapshot.child("name/last").getValue();
		        System.out.println("User " + userName + " now has a name of " + firstName + " " + lastName);
		    }

		    @Override
		    public void onChildRemoved(DataSnapshot snapshot) {
		        String userName = snapshot.getName();
		        GenericTypeIndicator<Map<String, Object>> t = new GenericTypeIndicator<Map<String, Object>>() {};
		        @SuppressWarnings("unused")
				Map<String, Object> userData = snapshot.getValue(t);
		        System.out.println("User " + userName + " has left the chat.");
		    }

		    @Override
		    public void onChildMoved(DataSnapshot snapshot, String previousChildName) {
		        String userName = snapshot.getName();
		        System.out.println("User " + userName + " changed priority, now is after " + previousChildName);
		    }

		    @Override
		    public void onCancelled() {
		    	System.out.println ("listener removed.");

		    }
		};
		usersRef.addChildEventListener(listener);
		Aquabase rootRef = new Aquabase ("https://aquabase/testroot/");
		rootRef.addChildEventListener(new ChildEventListener () {

			@Override
			public void onChildAdded(DataSnapshot snapshot,
					String previousChildName) {
				//System.out.println ("root added: " + snapshot.toString());
				
			}

			@Override
			public void onChildChanged(DataSnapshot snapshot,
					String previousChildName) {
				//System.out.println ("root changed: " + snapshot.toString());
				
			}

			@Override
			public void onChildRemoved(DataSnapshot snapshot) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onChildMoved(DataSnapshot snapshot,
					String previousChildName) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onCancelled() {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		
		usersRef.child("superman").setValue(getValue ("Clark", "Superman"));				
		usersRef.child("wilma").setValue(getValue ("Wilma", "Flinstone"));		
		usersRef.child("wilma").setValue(getValue ("Wilma", "Nosurname"));	
		usersRef.child("fred").setPriority(9999);
		
		usersRef.child("fred").removeValue();
		usersRef.removeEventListener(listener);
		
		
		Thread.sleep(100);		
		PersistenceManagerFactory.get().close();			
		
	}
	
}
