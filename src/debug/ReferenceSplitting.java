package debug;

import com.tomting.aquabase.client.persistence.Reference;

public class ReferenceSplitting {

	public static void main(String[] args) throws Exception {
		Reference test = new Reference ("https://www.test-chat.com/test/pippo");
		test.setPriority(234.5456456);
		System.out.println ("root: " + test.getRoot());
		System.out.println ("name: " + test.getName());
		System.out.println ("nspc: " + test.getNamespace());
		System.out.println ("refr: " + test.getStringReference());
		System.out.println ("prt : " + test.getComparableKey());
		

		Reference test2 = test.addChild("leaf");
		test2.setPriority("prova");
		System.out.println ("root: " + test2.getRoot());
		System.out.println ("name: " + test2.getName());
		System.out.println ("nspc: " + test2.getNamespace());
		System.out.println ("refr: " + test2.getStringReference());		
		System.out.println ("prt : " + test2.getComparableKey());	
		
		String toClean = test2.getComparableKey();
		System.out.println (toClean);
	}
}
