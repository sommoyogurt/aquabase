package debug;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tomting.aquabase.client.persistence.Reference;
import com.tomting.aquabase.client.persistence.ReferenceTree;
import com.tomting.aquabase.common.helpers.SimpleSerializer;

public class JacksonJsonTree {

	static Map<Reference, Object> getList (Reference reference, Object value) throws JsonProcessingException {
		Map<Reference, Object> result = null;
		
		if (value instanceof String || value instanceof Boolean || 
			value instanceof Double || value instanceof Long) {
			result = new TreeMap<Reference, Object> ();
			result.put(reference, value);
		} if (value instanceof List<?>) {
			result = new TreeMap<Reference, Object> ();
			result.put(reference, SimpleSerializer.serialize((Serializable) value));
		} else {
			ObjectMapper jsonWorker = new ObjectMapper ();
			String json = jsonWorker.writeValueAsString(value);
			System.out.println ("json: " + json);
			ReferenceTree tree = new ReferenceTree ();
			tree.fromJson(json);
			result = tree.toReference(reference);
		}
		return result;
	}
	
	/**


    List<Object>
	 * 
	 * @param args
	 * @throws JsonProcessingException 
	 */
	public static void main(String[] args) throws JsonProcessingException {
		
		/*
		Map<String, Object> test = new TreeMap<String, Object> ();
		test.put("test1", "val1");
		test.put("test2", "val2");	*/	
		
		/*
		Map<String, Message> test = new TreeMap<String, Message> ();
		Message value = new Message ("fred", "ciao");
		test.put("test1", value);
		test.put("test2", value);*/		
		
		List<String> test = new ArrayList<String> ();
		test.add("test");
		test.add("test2");
		
		Map<Reference, Object> result = getList (new Reference ("https://aquabase/root"), test);
		for (Entry<Reference, Object> r : result.entrySet()) {
			System.out.println (r.getKey().getComparableKey() + " - " + r.getValue());
		}

	}

}
