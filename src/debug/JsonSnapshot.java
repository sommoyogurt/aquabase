package debug;

import java.util.Map;

import com.tomting.aquabase.client.core.Aquabase;
import com.tomting.aquabase.client.core.DataSnapshot;
import com.tomting.aquabase.client.core.GenericTypeIndicator;
import com.tomting.aquabase.client.persistence.Reference;
import com.tomting.aquabase.client.persistence.ReferenceTree;

public class JsonSnapshot {


	public static void main(String[] args) {
		String json = "{\"fred\":{\"first\":\"Giancarlo\"}}";
		System.out.println ("input: " + json);
		ReferenceTree tree = new ReferenceTree ();
		tree.fromJson(json);
		System.out.println ("double check: " + tree.toJson(false));
		DataSnapshot snapshot = new DataSnapshot (new Aquabase (new Reference ("http://aquabase")), tree);
		System.out.println ("snap: " + snapshot.getName());
        GenericTypeIndicator<Map<String, Object>> t = new GenericTypeIndicator<Map<String, Object>>() {};
		Map<String, Object> userData = snapshot.getValue(t);		
		System.out.println ("mapping " + userData.get("first"));
		
		DataSnapshot subSnapshot = snapshot.child("fred/first");
		if (subSnapshot != null) System.out.println ("Risultato " + subSnapshot.getValue());
		
	}

}
