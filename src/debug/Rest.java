package debug;

import com.tomting.aquabase.server.servlet.ServletHandlerData;
import com.tomting.aquabase.server.servlet.ServletImplementation;
import com.tomting.aquabase.server.servlet.ServletImplementation.CallbackValue;

public class Rest {
	
	
	public static void main(String[] args) throws Exception {
		ServletHandlerData data = new ServletHandlerData ();
		data.setPath("/aquabase/users/fred.json");
		new ServletImplementation ().run(CallbackValue.GETVALUE, data);
		System.out.println (data.getOutput());
		Thread.sleep(1000);
		data.close();
	}
}
