package debug;

import java.util.ArrayList;
import java.util.List;

import com.tomting.aquabase.client.core.Aquabase;
import com.tomting.aquabase.client.core.DataSnapshot;
import com.tomting.aquabase.client.core.GenericTypeIndicator;
import com.tomting.aquabase.client.core.ValueEventListener;
import com.tomting.aquabase.client.persistence.PersistenceManagerFactory;
import com.tomting.aquabase.common.constants.AquabaseError;

public class LocalTest {


	
	public static void main(String[] args) throws Exception {
		
		Aquabase.goOffline();
		System.out.println ("start");
		Aquabase test = new Aquabase ("https://aquabase/message_list/message1/author");
		test.setValue("wilma", new Aquabase.CompletionListener () {

			@Override
			public void onComplete(AquabaseError error) {
				System.out.println ("scritto");
				
			}
		});
		test = new Aquabase ("https://aquabase/message_list/message1/text");
		test.setValue("hello!", new Aquabase.CompletionListener () {

			@Override
			public void onComplete(AquabaseError error) {
				System.out.println ("scritto");
				
			}
		});		
		Thread.sleep(100);
		
		test = new Aquabase ("https://aquabase/message_list/message1");
		test.addValueEventListener(new ValueEventListener () {

			@Override
			public void onDataChange(DataSnapshot snapshot) {

				Message m = snapshot.getValue (Message.class);
				System.out.println ("valore " + m.getAuthor());
			}

			@Override
			public void onCancelled() {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		Aquabase listRef = new Aquabase ("https://aquabase/message_list/fanculolimita");
		List<Message> supertest = new ArrayList<Message> ();
		supertest.add(new Message ("wilma", "hi"));
		supertest.add(new Message ("fred", "how are you"));
		
		listRef.child("message_complex").setValue(supertest);			
		test = new Aquabase ("https://aquabase/message_list/fanculolimita/message_complex");
		test.addValueEventListener(new ValueEventListener () {

			@Override
			public void onDataChange(DataSnapshot snapshot) {
				
			     GenericTypeIndicator<List<Message>> t = new GenericTypeIndicator<List<Message>>() {};
			     List<Message> messages = snapshot.getValue(t);				
			     System.out.println (messages.get(1).getText());
				
			}

			@Override
			public void onCancelled() {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		Thread.sleep(100);		
		PersistenceManagerFactory.get().close();		
	}
}
