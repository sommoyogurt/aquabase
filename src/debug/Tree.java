package debug;

import java.util.Map.Entry;

import com.tomting.aquabase.client.persistence.Reference;
import com.tomting.aquabase.client.persistence.ReferenceTree;



public class Tree {

	public static void main(String[] args) {

		ReferenceTree tree = new ReferenceTree ();
		tree.addSubReference ("users/fred/name/first", "Fred");
		tree.addSubReference ("users/fred/name/last", "Flinstone");		
		System.out.println ("r: " + tree.toJson(false));
		
		String reverse = "{\"fred\":{\"name\":{\"first\": \"Fred\",\"last\":5.6}}}";
		//String reverse = "{\"name\": {\"first\": \"Tom\"},\".priority\": 1.0}";
		System.out.println (reverse);
		tree = new ReferenceTree ();
		tree.fromJson(reverse);
		System.out.println ("r: " + tree.toJson(false));		
		
		Reference reference = new Reference ("https://www.test.bo");

		for (Entry<Reference, Object> s : tree.toReference(reference).entrySet()) {
			System.out.println (s.getKey().getComparableKey() + " " + 
								s.getValue() + " -> " + new Reference (s.getKey().getComparableKey()).getName()
								+ " -> " + new Reference (s.getKey().getComparableKey()).getComparableKey());
			
		}
		System.out.println ("***********************************");
		reference = new Reference ("https://www.test.bo/users/test/long/tom");		
		System.out.println ("root: " + reference.getRoot());
		System.out.println ("name: " + reference.getName());
		System.out.println ("ref:  " + reference.getStringReference());		
		System.out.println ("key:  " + reference.getComparableKey());	
		System.out.println ("***********************************");		
		
		tree = new ReferenceTree ();
		tree.fromJson("{\".value\":\"Tom\",\".priority\":1.0}");
		//tree.fromJson("{\".value\":\"Tom\"}");
		System.out.println ("r: " + tree.toJson(false));	
		for (Entry<Reference, Object> s : tree.toReference(reference).entrySet()) {
			System.out.println (s.getKey().getComparableKey() + " (" + s.getKey().getName() + ") V(" +
								s.getValue() + ") -> " + 
								new Reference (s.getKey().getComparableKey()).getName()
								+ " -> " + 
								new Reference (s.getKey().getComparableKey()).getComparableKey());
			
		}
		reference = new Reference ("ciccio/test/lavoro");
		System.out.println (reference.getName());
		
		
		
		
		/*
		NavigableMap<String, Object> test = new TreeMap<String, Object>  ();
		test.put("test", null);
		test.put("anvedi",null);
		test.put("bingo", null);
		test.put("$bingo", null);
		
		for ( Entry<String, Object> e : test.entrySet()) {
			System.out.println (e.getKey());
		}*/
		
		
		
		
	}

}
