package debug;

import com.tomting.aquabase.client.core.Aquabase;
import com.tomting.aquabase.client.persistence.PersistenceManagerFactory;
import com.tomting.aquabase.common.constants.AquabaseError;


public class Sandbox {

	public static void main(String[] args) throws Exception {

		//LOGIN TASK
		/*
		Aquabase sampleChatRef = new Aquabase ("https://SampleChat.firebaseIO-demo.com");
		SimpleLogin authClient = new SimpleLogin (sampleChatRef);	
		authClient.createUser("email@example.com", "very secret", new SimpleLoginAuthenticatedHandler () {

			@Override
			public void authenticated(Error error, User user) {
				if (error != null) {
					System.out.println ("not logged");
				} else {
					System.out.println ("logged");
				}
				
			}
			
		});*/
		
		//WRITER TASK
		Aquabase sampleChatRef = new Aquabase ("https://aquabase/users/fred/name");
		Aquabase first = sampleChatRef.child("first");
		first.setValue("Fred", new Aquabase.CompletionListener() {
			
			@Override
			public void onComplete(AquabaseError error) {
				System.out.println ("ok");
				
			}
		});
		Aquabase last = sampleChatRef.child("last");
		last.setValue("Sparrow", new Aquabase.CompletionListener() {
			
			@Override
			public void onComplete(AquabaseError error) {
				System.out.println ("ok");
				
			}
		});		
		
	
		
		
		
		
		Thread.sleep(1000);
		
		PersistenceManagerFactory.get().close();
		
		//DISCONNECT TASK
		/*
		fredRef.onDisconnect().setValue(ServerValue.TIMESTAMP);
		*/

		
		

		
		
		
	}

}
