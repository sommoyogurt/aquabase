package debug;

import com.tomting.aquabase.client.core.Aquabase;
import com.tomting.aquabase.client.core.DataSnapshot;
import com.tomting.aquabase.client.core.ValueEventListener;
import com.tomting.aquabase.client.persistence.PersistenceManagerFactory;

public class SimpleTest {
	public static void main(String[] args) throws Exception {
		Aquabase nameRef = new Aquabase ("https://SampleChat.firebaseIO-demo.com/users/fred/name");
		Aquabase.goOffline();
		nameRef.child("first").setValue("Fred");
		String url = "https://SampleChat.firebaseIO-demo.com/users/fred/name/first";
		Aquabase dataRef = new Aquabase (url);
		dataRef.addValueEventListener(new ValueEventListener() {
		    @Override
		    public void onDataChange(DataSnapshot snapshot) {
		        System.out.println("Fred's first name is " + snapshot.getValue());
		    }

		    @Override
		    public void onCancelled() {
		        System.err.println("Listener was cancelled");
		    }


		});		
		
		System.out.println ("push: " + dataRef.push().getName());
		

		
		Thread.sleep(100);		
		PersistenceManagerFactory.get().close();				
	}
}
