package com.tomting.aquabase.client.persistence;

import java.util.Map;

import com.tomting.aquabase.client.core.ChildEventListener;
import com.tomting.aquabase.client.core.ValueEventListener;
import com.tomting.aquabase.client.core.Aquabase.CompletionListener;


public class PersistenceManager {	
	private static LocalManager localManager = new LocalManager ();	
	private static RemoteManager remoteManager = new RemoteManager ();	
	
	public PersistenceManager () {}
	
	/**
	 * Skip thrift error
	 */
	public void close () {
		
		remoteManager.close();
	}
	
	/**
	 * Legacy setValue
	 * it seems like setValue is updating objects without splitting them to subkeys/values
	 * @param listener
	 */
	public void setValue (Reference reference, Object object, CompletionListener listener) {
		
		localManager.setValue(reference, object);
		remoteManager.setValue(reference, object, listener);		
	}
	
	/**
	 * Legacy updateChildren
	 * @param children
	 */
	public void updateChildren (Reference reference, Map<String, Object> children) {
		
		localManager.updateChildren(reference, children);
		remoteManager.updateChildren(reference, children);		
	}
	
	/**
	 * Legacy removeValue
	 * @param reference
	 * @param listener
	 */
	public void removeValue (Reference reference, CompletionListener listener) {
		
		localManager.removeValue(reference);
		remoteManager.removeValue(reference, listener);		
	}
	
	/**
	 * Legacy goOffline
	 */	
	public void goOnline () {
		
		remoteManager.goOnline();
	}
	
	/**
	 * Legacy goOffline
	 */	
	public void goOffline () {
		
		remoteManager.goOffline();
	}	
	
	/**
	 * Legacy addValueEventListener
	 * @param valueEventListener
	 */
	public void addValueEventListener (Reference reference, ValueEventListener listener) {
		
		localManager.addValueEventListener(reference, listener);
	}
	
	/**
	 * Legacy addChildEventListener
	 * @param listener
	 */
	public void addChildEventListener (Reference reference, ChildEventListener listener) {
		
		localManager.addChildEventListener(reference, listener);
	}
	
	/**
	 * Legacy removeEventListener
	 * @param valueEventListener
	 */
	public void removeEventListener (Reference reference, ValueEventListener listener) {
		
		localManager.removeEventListener(reference, listener);
	}
	
	/**
	 * Legacy removeEventListener
	 * @param listener
	 */
	public void removeEventListener (Reference reference, ChildEventListener listener) {
		
		localManager.removeEventListener(reference, listener);
	}
	
}
