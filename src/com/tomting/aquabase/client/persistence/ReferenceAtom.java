package com.tomting.aquabase.client.persistence;

public class ReferenceAtom {
	private String name;
	private String priority;
	
	public ReferenceAtom (String name) {
		this.setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}
}