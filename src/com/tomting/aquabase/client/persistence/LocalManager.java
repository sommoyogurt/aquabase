package com.tomting.aquabase.client.persistence;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tomting.aquabase.client.core.Aquabase;
import com.tomting.aquabase.client.core.ChildEventListener;
import com.tomting.aquabase.client.core.ChildEventRunnable;
import com.tomting.aquabase.client.core.DataSnapshot;
import com.tomting.aquabase.client.core.ValueEventListener;
import com.tomting.aquabase.client.core.ValueEventRunnable;
import com.tomting.aquabase.common.helpers.SimpleSerializer;

public class LocalManager extends LocalReplica {
	private CallbackReplica<ChildEventListener> callbackReplicaChildEvent = new CallbackReplica<ChildEventListener> ();
	private CallbackReplica<ValueEventListener> callbackReplicaValueEvent = new CallbackReplica<ValueEventListener> ();
		
	/**
	 * Helper: split value to keys following Firebase/Jackson paradigm
	 * @param reference
	 * @param value
	 * @return
	 * @throws JsonProcessingException
	 */
	static Map<Reference, Object> splitValues (Reference reference, Object value) {
		Map<Reference, Object> result = null;
		
		if (value instanceof String || value instanceof Boolean || 
			value instanceof Double || value instanceof Long) {
			result = new TreeMap<Reference, Object> ();
			result.put(reference, value);
		} if (value instanceof List<?>) {
			result = new TreeMap<Reference, Object> ();
			result.put(reference, SimpleSerializer.serialize((Serializable) value));
		} else {
			try {
				ObjectMapper jsonWorker = new ObjectMapper ();
				String json = jsonWorker.writeValueAsString(value);
				ReferenceTree tree = new ReferenceTree ();
				tree.fromJson(json);
				result = tree.toReference(reference);
			} catch (Exception e) {}
		}
		return result;
	}	
	
			
	/**
	 * Helper: ChildEventListener thread
	 * @param listener
	 * @param action
	 * @param snapshot
	 * @param previousChildName
	 */
	private void runChildEventThread (ChildEventListener listener, ChildEventRunnable.Action action, DataSnapshot snapshot, String previousChildName) {
		
		(new Thread(new ChildEventRunnable(listener, action, snapshot, previousChildName))).start();			
	}
	
	
	/**
	 * Helper: ValueEventListener thread
	 * @param listener
	 * @param action
	 */
	private void runValueEventThread (ValueEventListener listener, ValueEventRunnable.Action action, DataSnapshot snapshot) {
		
		(new Thread(new ValueEventRunnable(listener, action, snapshot))).start();			
	}	

	/**
	 * Helper read
	 * @param reference
	 * @return
	 */
	private DataSnapshot read (Reference reference) {
		
		Reference parent = reference.getParent();	
		ReferenceTree tree = new ReferenceTree ();
		for (Entry<Reference, Object> e : readList (reference)) 
			tree.addReference(e.getKey(), e.getValue());		
		return new DataSnapshot (new Aquabase (parent), tree.child(parent.getName()));
	}
	
	/**
	 * Helper write
	 * denoising: keep the first previousReference only
	 * @param reference
	 * @param value
	 * @param listener
	 * @param update
	 */
	private synchronized void write (Reference reference, Object value, boolean update) {
		
		boolean addedOrChanged = readList (reference, false).size() == 0;
		if (!update) atomicDelete (reference);
		Reference candidatePreviousReference = null;
		Reference parent = reference.getParent();
		for (Entry<Reference, Object> e : splitValues (reference, value).entrySet()) {
			Reference writedPreviousReference = atomicWrite (e.getKey(), e.getValue());
			if (candidatePreviousReference == null) candidatePreviousReference = writedPreviousReference;	
		}		
		String previousChildName = 
			candidatePreviousReference != null ? candidatePreviousReference.getSubReference(parent).limitNameDepth (1).getName () : null;	
		Reference callbackReference = reference;	
		do {
			for (ChildEventListener c : callbackReplicaChildEvent.getCallbacks(callbackReference.getParent())) 
				runChildEventThread (c, 
						addedOrChanged ? ChildEventRunnable.Action.ONCHILDADDED : ChildEventRunnable.Action.ONCHILDCHANGED, 
						read (callbackReference), previousChildName);
			callbackReference = callbackReference.getParent();
			addedOrChanged = false;
		} while (callbackReference.getNames().size() > 0);
	}	
	
	/**
	 * Legacy setValue
	 * it seems like setValue is updating objects without splitting them to subkeys/values
	 * @param listener
	 */
	public void setValue (Reference reference, Object value) {
		
		write (reference, value, false);		
	}
	
	/**
	 * Legacy updateChildren
	 * @param children
	 */
	public void updateChildren (Reference reference, Map<String, Object> children) {
			
		write (reference, children, true);	
	}
	
	/**
	 * Legacy removeValue
	 * @param reference
	 * @param listener
	 */
	public void removeValue (Reference reference) {
		
		DataSnapshot snapshot = atomicDelete (reference); 
		for (ChildEventListener c : callbackReplicaChildEvent.getCallbacks(reference.getParent())) 
			runChildEventThread (c, ChildEventRunnable.Action.ONCHILDREMOVED, snapshot, null);			
	}
	
	/**
	 * Legacy addValueEventListener
	 * @param valueEventListener
	 */
	public void addValueEventListener (Reference reference, ValueEventListener listener) {
		
		listener.onDataChange(atomicRead (reference));
	}	
	
	/**
	 * Legacy addChildEventListener
	 * denoising: prevent the same previousChildName twice
	 * @param listener
	 */
	public void addChildEventListener (Reference reference, ChildEventListener listener) {
		
		String previousChildName = null;
		for (Entry<Reference, Object> e : readList (reference, true)) {
			DataSnapshot snapshot = new DataSnapshot (
					new Aquabase (reference), 
					new ReferenceTree (e.getKey(), e.getValue()));
			String thisChildName = e.getKey().limitNameDepth(1).getName();
			if (!thisChildName.equals(previousChildName))
				runChildEventThread (listener, ChildEventRunnable.Action.ONCHILDADDED, snapshot, previousChildName);		
			previousChildName = thisChildName;
		}
		callbackReplicaChildEvent.addEventListener(reference, listener);
	}
	
	/**
	 * Legacy removeEventListener
	 * @param valueEventListener
	 */
	public void removeEventListener (Reference reference, ValueEventListener listener) {
	
		if (callbackReplicaValueEvent.removeCallback(reference, listener)) 
			runValueEventThread (listener, ValueEventRunnable.Action.ONCANCELLED, null);	
	}
	
	/**
	 * Legacy removeEventListener
	 * @param listener
	 */
	public void removeEventListener (Reference reference, ChildEventListener listener) {
		
		if (callbackReplicaChildEvent.removeCallback(reference, listener)) 
			runChildEventThread (listener, ChildEventRunnable.Action.ONCANCELLED, null, null);	
	}		
	
	
}
