package com.tomting.aquabase.client.persistence;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

import com.tomting.aquabase.client.core.Aquabase;
import com.tomting.aquabase.client.core.DataSnapshot;

public class LocalReplica {
	private ConcurrentNavigableMap<String, String> localPriorities = new ConcurrentSkipListMap<String, String> ();	
	private ConcurrentNavigableMap<String, Object> localReplica = new ConcurrentSkipListMap<String, Object> ();
	
	/**
	 * Helper: next key for quick&dirty range query implementation
	 * @param key
	 * @return
	 */
	private static String nextKey (String key) {
		
		return key + "\u0000";
	}	
	
	/**
	 * Delete
	 * @param reference
	 */
	private boolean delete (Reference reference) {	
		
		String name = reference.getName();
		String comparableKey = localPriorities.get(name);
		if (comparableKey != null) {
			localPriorities.remove(name);
			localReplica.remove(comparableKey);	
			return true;
		}
		return false;
	}
	
	/**
	 * Write
	 * @param reference
	 * @param object
	 * @return
	 */
	private Reference write (Reference reference, Object object) {	
				
		String comparableKey = reference.getComparableKey();
		localPriorities.put(reference.getName(), comparableKey);
		String previousChildName = localReplica.floorKey (comparableKey);			
		localReplica.put(comparableKey, object);
		return previousChildName != null ? new Reference (previousChildName) : null;	
	}

	
	/**
	 * Write
	 * @param reference
	 * @param object
	 */
	protected Reference atomicWrite (Reference reference, Object object) {
		
		synchronized (localReplica) {
			delete (reference);
			return write (reference, object);
		}
	}
	
	/**
	 * Helper: read a list of maps
	 * @param reference
	 * @return
	 */
	protected List<Map.Entry <Reference, Object>> readList (Reference reference) {
		
		List<Map.Entry <Reference, Object>>  result = new ArrayList<Map.Entry <Reference, Object>> ();
		String floorKey = reference.getComparableKey();	
		String ceilKey = Reference.ceilKey(floorKey);
		do {
			Entry<String, Object> entry = localReplica.ceilingEntry (floorKey);			
			if (entry == null || entry.getKey().compareTo(ceilKey) >= 0) break;
			floorKey = nextKey (entry.getKey());				
			result.add(new AbstractMap.SimpleEntry<Reference, Object>
				(new Reference (entry.getKey()), entry.getValue()));
		} while (true);
		return result;
	}	
	
	/**
	 * Helper: read a list of maps
	 * @param reference
	 * @return
	 */
	protected List<Map.Entry <Reference, Object>> readList (Reference reference, boolean includedFilter) {
	
		List<Map.Entry <Reference, Object>>  result = new ArrayList<Map.Entry <Reference, Object>> ();
		Reference filter = includedFilter ? reference : reference.getParent();	
		for (Entry<Reference, Object> e : readList (reference)) 
			result.add(new AbstractMap.SimpleEntry<Reference, Object>
				(new Reference (e.getKey()).getSubReference(filter), e.getValue()));
		return result;
	}
	
	/**
	 * Read
	 * @param reference
	 */
	private DataSnapshot read (Reference reference, boolean includedFilter) {
			
		List<Map.Entry <Reference, Object>> queryResult = readList (reference, includedFilter);
		ReferenceTree tree = new ReferenceTree ();
		for (Entry<Reference, Object> e : queryResult) tree.addReference(e.getKey(), e.getValue());
		return new DataSnapshot (new Aquabase (reference), tree);
	}	
	
	/**
	 * Read 
	 * @param reference
	 * @param object
	 */
	protected DataSnapshot atomicRead (Reference reference) {
		
		synchronized (localReplica) {
			return read (reference, true);
		}
	}	
	
	/**
	 * Delete
	 * @param reference
	 * @param object
	 */
	protected DataSnapshot atomicDelete (Reference reference) {
		
		synchronized (localReplica) {
			DataSnapshot snapshot = read (reference, false);			
			delete (reference);
			return snapshot;
		}
	}	
	
	
}
