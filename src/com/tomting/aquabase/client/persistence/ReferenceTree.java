package com.tomting.aquabase.client.persistence;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

public class ReferenceTree {
	public class ObjectDeserializer implements JsonDeserializer<Object> {

	    @Override
	    public Object deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
	    	return element.toString();	
	    }
	}	
	
	private Map<String, Object> tree = new TreeMap<String,Object>(); 
	private Gson gson = new Gson ();
	private Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();
	private static final String TRUE = "true";
	private static final String FALSE = "false";	
	private static final String priorityJsonTag = ".priority";
	private static final String valueJsonTag = ".value";	
	
	public ReferenceTree () {}
	
	public ReferenceTree (Reference reference, Object value)  {
		
		addReference (reference, value);
	}	
	
	protected ReferenceTree (Map<String, Object> tree) {
		
		this.tree = tree;
	}
	
	/**
	 * Recursive population
	 * @param inputTree
	 * @param names
	 * @param value
	 */
	@SuppressWarnings("unchecked")
	private void populateFromReference (Map<String, Object> inputTree, List<ReferenceAtom> names, int level, Object value) {
		Object nextValue;
	
		String insertKey = names.size() != 0 ? names.get(level).getName() : valueJsonTag;
		if (level >= names.size() - 1) nextValue = value;
		else {
			Map<String, Object> newTree = (Map<String, Object>) inputTree.get(insertKey);
			if (newTree == null) newTree = new TreeMap<String,Object>(); 		
			populateFromReference (newTree, names, level + 1, value);
			nextValue = newTree;
		}
		inputTree.put(insertKey, nextValue);
	}
	
	/**
	 * Add leaf 
	 * @param reference
	 */
	public void addReference (Reference reference, Object value) {
		
		populateFromReference (tree, reference.getNames (), 0, value);
	}
	
	/**
	 * Add leaf 
	 * @param reference
	 */
	public void addSubReference (String subReference, Object value) {
		
		populateFromReference (tree, Reference.split(subReference), 0, value);
	}	
	
	/**
	 * Get json representation
	 * @return
	 */
	public String toJson (boolean pretty) {
		
		return (pretty ? prettyGson : gson).toJson(tree);
	}
	
	/**
	 * Recursive population
	 * @param json
	 * @return
	 */
	private Object populateFromJson (String json) {

		final Gson gson = new GsonBuilder().registerTypeAdapter(Object.class, new ObjectDeserializer()).create();
		Type mapType = new TypeToken<Map<String, Object>>() {}.getType();
		Map<String, Object> map = gson.fromJson(json, mapType);
		for (Entry<String, Object> m : map.entrySet()) {	
			try {
				if (TRUE.equals(m.getValue()) || FALSE.equals(m.getValue())) {
					m.setValue(Boolean.valueOf((String) m.getValue()));
					continue;
				}
			} catch (Exception e) {}			
			
			try {
				m.setValue(Integer.valueOf((String) m.getValue()));
				continue;
			} catch (Exception e) {}			
			try {
				m.setValue(Double.valueOf((String) m.getValue()));
				continue;
			} catch (Exception e) {}			
			try {
				m.setValue(populateFromJson (m.getValue().toString()));
				continue;
			} catch (Exception e) {}		
			m.setValue(String.valueOf(m.getValue()).trim().replaceAll("^\"|\"$", ""));			
		}		
		return map;
	}
	
	@SuppressWarnings("unchecked")
	public void fromJson (String json) {
		
		tree = (Map<String, Object>) populateFromJson (json); 
	}
	
	/**
	 * Recursive population
	 * @param inputTree
	 * @param root
	 * @return
	 */

	@SuppressWarnings("unchecked")
	private Map<Reference, Object> populateToRefence (Map<String, Object> inputTree, Reference root) {
		Map<Reference, Object> map = new TreeMap<Reference, Object> ();
		Object priority = null;
			
        List<Map.Entry<String, Object>> sortedInputTree = new LinkedList<Map.Entry<String, Object>>(inputTree.entrySet() );
        Collections.sort(sortedInputTree, new Comparator<Map.Entry<String, Object>> () {

			@Override
			public int compare(Entry<String, Object> arg0,
					Entry<String, Object> arg1) {
				return (arg0.getKey().compareTo(arg1.getKey()));
			}
        });		
		
		for (Entry<String, Object> m : sortedInputTree) {
			Reference newReference;
			if (m.getKey().equals(valueJsonTag)) newReference = root;
			else newReference = new Reference (root.addChild(m.getKey()));
			
			if (priority != null) newReference.setPriority(priority);
			if (m.getKey().equals(priorityJsonTag)) {
				priority = m.getValue();
				continue;
			}
			if (m.getValue() instanceof Map) map.putAll(populateToRefence ((Map<String, Object>) m.getValue(), newReference));
			else map.put(newReference, m.getValue());
		}
		return map;
	}
	
	/**
	 * Create list of references starting from a root
	 * @param reference
	 * @return
	 */
	public Map<Reference, Object> toReference (Reference reference) {
		
		return populateToRefence (tree, reference);
	}
	
	/**
	 * Helper: get single value from a node
	 * @return
	 */
	public Object getValue () {
		
		Set<Entry<String, Object>> entrySet = tree.entrySet();
		if (entrySet.size() == 1) return entrySet.iterator().next().getValue();
		else return null;
	}
	
	/**
	 * Helper: get single value from a node in ReferenceTree fashion
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ReferenceTree getValueTree () {
		
		return new ReferenceTree ((Map<String, Object>) getValue ());
	}
	
	/**
	 * Helper: get single value from a node
	 * @return
	 */
	public String getKey () {
		Set<Entry<String, Object>> entrySet = tree.entrySet();
		if (entrySet.size() == 1) return entrySet.iterator().next().getKey();
		else return null;
	}	
	
	/**
	 * Child navigation
	 * @param path
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ReferenceTree child (String path) {
			
		Map<String, Object> node = tree;		
		Reference subPath = new Reference (path);
		for (ReferenceAtom n : subPath.getNames ()) {
			Object value = node.get(n.getName());
			if (value instanceof Map) node = (Map<String, Object>) value;
			else {
				node = new TreeMap<String, Object> ();
				node.put(valueJsonTag, value);			
			}
		}
		return new ReferenceTree (node);
	}
	
}
