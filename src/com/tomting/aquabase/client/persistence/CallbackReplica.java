package com.tomting.aquabase.client.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CallbackReplica <T> {
	private HashMap<String, List<T>> localMap = new HashMap<String, List<T>> ();

	public CallbackReplica () {}
	
	/**
	 * Synchronized write
	 * @param key
	 * @param t
	 */
	public void addEventListener (Reference key, T t) {

		synchronized (localMap) {
			List<T> list = localMap.get(key.getName());
			if (list == null) list = new ArrayList<T> ();
			list.add(t);
			localMap.put(key.getName(), list);
		}
	}
	
	/**
	 * Read a key (concurrent)
	 * @param key
	 * @return
	 */
	public List<T> getCallbacks (Reference key) {

		synchronized (localMap) {			
			List<T> result = localMap.get(key.getName());
			return result == null ? new ArrayList<T> () : result;
		}
	}
	
	/**
	 * Legacy removeCallback
	 * @param key
	 * @param callback
	 * @return
	 */
	public boolean removeCallback (Reference key, T callback) {
		
		synchronized (localMap) {	
			return getCallbacks (key).remove(callback);		
		}
	}
}
