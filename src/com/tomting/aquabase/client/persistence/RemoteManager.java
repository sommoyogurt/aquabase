package com.tomting.aquabase.client.persistence;

import java.io.Serializable;
import java.util.Map;

import com.tomting.aquabase.ThriftWrite;
import com.tomting.aquabase.ThriftWriteResult;
import com.tomting.aquabase.client.core.Aquabase.CompletionListener;
import com.tomting.aquabase.client.thrift.NioSetValueBulkExecutor;
import com.tomting.aquabase.client.thrift.NioSetValueServiceTransport;
import com.tomting.aquabase.client.thrift.SocketFactory;
import com.tomting.aquabase.common.constants.Constants;
import com.tomting.aquabase.common.helpers.SimpleSerializer;
import com.tomting.aquabase.common.helpers.nio.WorkerPool;

public class RemoteManager {
	private static final SocketFactory sf = new SocketFactory (Constants.getClientIntProperty(Constants.poolExpirationTime));	
	private static final WorkerPool<ThriftWrite, ThriftWriteResult, NioSetValueServiceTransport> workerPool = 
			new WorkerPool<ThriftWrite, ThriftWriteResult, NioSetValueServiceTransport> (
					true, 
					NioSetValueServiceTransport.class, 
					new NioSetValueBulkExecutor (sf), 
					Constants.getClientIntProperty(Constants.workersProperty), 
					Constants.getClientIntProperty(Constants.bulkSizeProperty));
	protected boolean online;
	
	public RemoteManager () {
		online = true;
		workerPool.addRef();
	}
	
	/**
	 * Skip thrift error
	 */
	public void close () {
		sf.close();
	}
	
	/**
	 * 
	 * @param reference
	 * @param serialized
	 * @param completionListener
	 */
	protected void sendToServer (Reference reference, Serializable object, CompletionListener completionListener) {

		NioSetValueServiceTransport job = 
			new NioSetValueServiceTransport (	reference.getNamespace (), 
												reference.getName(),  
												SimpleSerializer.serialize(object), 
												completionListener);
		try {
			workerPool.addJob(job);
		} catch (InterruptedException e) {}
	}	

	/**
	 * Legacy goOffline
	 */	
	public void goOnline () {
		online = true;
		workerPool.addRef();
	}
	
	/**
	 * Legacy goOffline
	 */
	public void goOffline () {
		online = false;
		workerPool.subRef();
	}	
	
	/**
	 * Legacy setValue
	 * @param reference
	 * @param object
	 * @param completionListener
	 */
	public void setValue (Reference reference, Object object, CompletionListener listener) {
		
		if (online) sendToServer (reference, (Serializable) object, listener);
		else if (listener != null) listener.onComplete(null);		
	}
	
	/**
	 * Legacy removeValue
	 * @param reference
	 * @param listener
	 */
	public void removeValue (Reference reference, CompletionListener listener) {
	
		if (online) {}
		else if (listener != null) listener.onComplete(null);			
	}
	
	/**
	 * Legacy updateChildren
	 * @param reference
	 * @param children
	 */
	public void updateChildren (Reference reference, Map<String, Object> children) {
		
		//check splitting
		if (online) sendToServer (reference, (Serializable) children, null);		
	}
}
