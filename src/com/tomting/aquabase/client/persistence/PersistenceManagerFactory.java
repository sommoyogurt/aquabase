package com.tomting.aquabase.client.persistence;

public class PersistenceManagerFactory {
	private static final PersistenceManager pm = new PersistenceManager ();
	
	private PersistenceManagerFactory () {}
	
	public static PersistenceManager get () {
		return pm;
	}
}
