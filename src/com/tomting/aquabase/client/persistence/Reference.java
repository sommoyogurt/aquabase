package com.tomting.aquabase.client.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.eaio.uuid.UUID;

public class Reference implements Comparable<Reference> {
	private static final String protocolSeparator = "://";	
	private static final String nameSeparator = "/";	
	private static final String prioritySeparatorStart = "[";
	private static final String prioritySeparatorEnd = "]";	
	
	private String root;
	private List<ReferenceAtom> names = new ArrayList<ReferenceAtom> ();
	
	public Reference (String stringReference) {
		
		if (stringReference.indexOf(protocolSeparator) < 0) 
			this.setStringReference(protocolSeparator + nameSeparator + stringReference);
		else this.setStringReference(stringReference);
	}
	
	public Reference (Reference reference) {
		
		this.root = reference.root;
		this.names = new ArrayList<ReferenceAtom> (reference.names);
	}
	
	private Reference (Reference reference, List<ReferenceAtom> names) {
				
		this.names = new ArrayList<ReferenceAtom> (names);
		this.root = reference.root;
	}

	/**
	 * Helper: convert to path
	 * @return
	 */
	public String getStringReference() {
		
		String result = root;
		for (ReferenceAtom a : names) result += nameSeparator + a.getName();
		return result;
	}
	
	/**
	 * Helper: convert to path including priority substrings
	 * @return
	 */
	public String getComparableKey () {
		
		String result = "";
		for (ReferenceAtom a : names){
			result += nameSeparator + a.getName();	
			if (a.getPriority() != null) result += prioritySeparatorStart + a.getPriority() + prioritySeparatorEnd;
		}
		return result.substring(1);
	}	
		
	/**
	 * Helper: convert path to internal representation
	 * @param stringReference
	 */
	private void setStringReference(String stringReference) {
				
		String[] temp = stringReference.split(Pattern.quote(protocolSeparator));
		String pathString = temp.length > 1 ? temp [1] : temp [0];		
		this.root =  pathString.split(Pattern.quote(nameSeparator)) [0];			
		String name = pathString.indexOf(nameSeparator) >= 0 ? pathString.substring(pathString.indexOf(nameSeparator) + 1) : "";		
		this.names = split (name);	
	}
		
	/**
	 * Get parent
	 * @return
	 */
	public Reference getParent () {
		
		return new Reference (this, this.names.subList(0, this.names.size() - 1));
	}
	
	/**
	 * Add a child
	 * @param pathString
	 * @return
	 */
	public Reference addChild (String pathString) {
		List<ReferenceAtom> names = new ArrayList<ReferenceAtom> (this.names);
		names.addAll(split (pathString));
		return new Reference (this, names);
	}
	
	/**
	 * Helper: return root (http address)
	 * @return
	 */
	public String getRoot () {
		
		return root;
	}
	
	/**
	 * Helper: return key without root
	 * @return
	 */
	public String getName () {

		if (names.size() == 0) return null;
		String result = "";		
		for (ReferenceAtom a : names) result += nameSeparator + a.getName();
		return result.substring(1);
	}	
		
	/**
	 * Helper: keep only word characters
	 * @return
	 */
	public String getNamespace () {
		
		return getRoot ().replaceAll("[^\\p{L}\\p{Nd}]", "");
	}
		
	public List<ReferenceAtom> getNames () {
		return this.names;
	}
	
	/**
	 * Legacy priority
	 * @param priority
	 */
	public void setPriority (Object priority) {
		
		if (!names.isEmpty()) {
			ReferenceAtom na = names.get(names.size() - 1);
			if (priority instanceof Double) na.setPriority(Double.toHexString((Double) priority));
			else na.setPriority(priority.toString());	
		}
	}
		
	/**
	 * Helper: split a path
	 * @param stringReference
	 * @return
	 */
	public static List<ReferenceAtom> split (String stringReference) {
		String [] splitNames = stringReference.split(Pattern.quote(nameSeparator));
		ArrayList<ReferenceAtom> result = new ArrayList<ReferenceAtom> ();
		
		for (String s : splitNames) {
			ReferenceAtom nameAtom = new ReferenceAtom (s);
			String [] calcPriorities = s.split(Pattern.quote(prioritySeparatorStart));
			if (calcPriorities.length > 1) {
				nameAtom.setName(calcPriorities [0]);
				nameAtom.setPriority(calcPriorities [1].substring(0, calcPriorities [1].length() - 1));
			}
			result.add(nameAtom);
		}
		return result;
	}

	/**
	 * Comparable: used for TreeMap
	 * @param other
	 * @return
	 */
	@Override
	public int compareTo(Reference other) {

		return this.getComparableKey ().compareTo(other.getComparableKey());
	}	
	
	/**
	 * Helper: range query upper bound
	 * @param floorKey
	 * @return
	 */
	public static String ceilKey (String floorKey) {
		return floorKey + nameSeparator + "\ufffd";
	}
	
	/**
	 * Helper: sub reference
	 * @param root
	 * @return
	 */
	public Reference getSubReference (Reference root) {
		
		Reference result = new Reference (this);
		result.root = null;	
		result.names = this.getNames().subList(root.names.size(), this.names.size());			
		return result;
	}
	
	/**
	 * Helper: limit name depth size
	 * @param length
	 * @return
	 */
	public Reference limitNameDepth (int length) {
		
		Reference result = new Reference (this);
		result.root = null;	
		result.names = this.getNames().subList(0, length);			
		return result;		
	}
	
	/**
	 * Legacy: push
	 * @return
	 */
	public Reference push () {
		
		List<ReferenceAtom> names = new ArrayList<ReferenceAtom> (this.names);
		names.add (new ReferenceAtom (new UUID().toString()));
		return new Reference (this, names);
	}
	
}
