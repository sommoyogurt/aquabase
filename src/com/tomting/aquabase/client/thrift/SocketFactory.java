package com.tomting.aquabase.client.thrift;

import java.io.IOException;

import org.apache.thrift.transport.TNonblockingSocket;

import com.tomting.aquabase.common.constants.Constants;
import com.tomting.orion.connection.ObjectPool;

public class SocketFactory extends ObjectPool<TNonblockingSocket> {
	private static String host = Constants.getClientStringProperty(Constants.thriftJavaHost);
	private static int port = Constants.getClientIntProperty(Constants.thriftJavaPort);
	
	public SocketFactory(long expirationTime) {
		super(expirationTime);
	}

	@Override
	protected TNonblockingSocket create() {

		try {
			return new TNonblockingSocket (host, port);
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	public void expire(TNonblockingSocket socket) {

		socket.close();
	}

	@Override
	public boolean validate(TNonblockingSocket socket) {

		return socket.isOpen();
	}

}
