package com.tomting.aquabase.client.thrift;

import java.util.List;

import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.transport.TNonblockingSocket;

import com.tomting.aquabase.ThriftAquabase.AsyncClient.setValue_call;
import com.tomting.aquabase.ThriftAquabase;
import com.tomting.aquabase.ThriftWrite;
import com.tomting.aquabase.ThriftWriteResult;
import com.tomting.aquabase.common.helpers.nio.BulkExecutor;
import com.tomting.aquabase.common.thrift.Helpers;

class SetValueCallback implements AsyncMethodCallback<ThriftAquabase.AsyncClient.setValue_call> {
	SocketFactory sf; 
	TNonblockingSocket socket;
	List<NioSetValueServiceTransport> bulkCallback;
		
	public SetValueCallback (SocketFactory sf, TNonblockingSocket socket, List<NioSetValueServiceTransport> bulkCallback) {
		this.sf = sf;
		this.socket = socket;
		this.bulkCallback = bulkCallback;
	}
	
	@Override
	public void onComplete(setValue_call callback) {		

		try {
			List<ThriftWriteResult> result = callback.getResult();
			for (int i = 0; i < result.size(); i++) bulkCallback.get(i).setResult(result.get(0));
		} catch (TException e) {
		}
	}

	@Override
	public void onError(Exception exception) {
		
		List<ThriftWriteResult> result = Helpers.getBulkWriteResult(bulkCallback.size());
		for (int i = 0; i < result.size(); i++) bulkCallback.get(i).setResult(result.get(0));
	}
	
}


public class NioSetValueBulkExecutor extends BulkExecutor<ThriftWrite, ThriftWriteResult, NioSetValueServiceTransport> {
	SocketFactory sf;
	
	public NioSetValueBulkExecutor (SocketFactory sf) {
		
		this.sf = sf;
	}

	/**
	 * Not used
	 */
	@Override
	public List<ThriftWriteResult> executeSync(List<ThriftWrite> bulkInput) {

		return null;
	}

	@Override
	public void executeAsync(List<ThriftWrite> bulkInput,
			List<NioSetValueServiceTransport> bulkCallback) {
		TNonblockingSocket socket = sf.checkOut();        
        try {
			Helpers.getAsyncClient(socket).setValue(bulkInput, new SetValueCallback (sf, socket, bulkCallback));
		} catch (Exception e) {	} 
	}
	
}
