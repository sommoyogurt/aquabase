package com.tomting.aquabase.client.thrift;

import com.tomting.aquabase.ThriftWrite;
import com.tomting.aquabase.ThriftWriteResult;
import com.tomting.aquabase.client.core.Aquabase.CompletionListener;
import com.tomting.aquabase.common.constants.AquabaseError;
import com.tomting.aquabase.common.constants.Constants;
import com.tomting.aquabase.common.helpers.nio.Job;
import com.tomting.aquabase.common.thrift.Helpers;

public class NioSetValueServiceTransport extends Job<ThriftWrite, ThriftWriteResult> {
	String namespace;
	String key;
	String value;
	boolean valid;
	CompletionListener listener;
	
	public NioSetValueServiceTransport () {
		valid = false;
	}
	
	public NioSetValueServiceTransport (String namespace, String key, String value, CompletionListener listener) {
		this.namespace = namespace;
		this.key = key;
		this.value = value;
		this.listener = listener;
		valid = true;
	}
	
	@Override
	public Job<ThriftWrite, ThriftWriteResult> create() {
		
		return new NioSetValueServiceTransport (); 
	}

	@Override
	public boolean isValid() {

		return valid;
	}

	@Override
	public ThriftWrite getService() {
		
		return Helpers.getWrite(namespace, key, value);
	}

	@Override
	public void setResult(ThriftWriteResult result) {	
		listener.onComplete(result.result ? null : new AquabaseError (Constants.serviceResultFalse));
	}

	@Override
	public void setResult(String error) {
		
		listener.onComplete(new AquabaseError (error));	
	}

	@Override
	public void syncReleaseLock() {		
	}

	@Override
	public void syncWaitLock() {		
	}	

	
}
