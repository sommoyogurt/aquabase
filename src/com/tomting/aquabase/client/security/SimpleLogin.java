package com.tomting.aquabase.client.security;

import com.tomting.aquabase.client.core.Aquabase;

public class SimpleLogin {
	public Aquabase ref;
	
	public SimpleLogin (Aquabase ref) {
		this.ref = ref;
	}
	
	/**
	 * Legacy createUser
	 * @param email
	 * @param password
	 * @param handler
	 */
	public void createUser (String email, String password, SimpleLoginAuthenticatedHandler handler) {
		//TODO
	}
	
	/**
	 * Legacy loginWithEmail
	 * @param email
	 * @param password
	 * @param handler
	 */
	public void loginWithEmail (String email, String password, SimpleLoginAuthenticatedHandler handler) {
		
	}	
	
	/**
	 * Legacy removeUser
	 * @param email
	 * @param password
	 * @param handler
	 */
	public void removeUser(String email, String password, SimpleLoginCompletionHandler handler) {
		
	}
	
	/**
	 * Legacy changePassword
	 * @param email
	 * @param oldPassword
	 * @param newPassword
	 * @param handler
	 */
	public void changePassword (String email, String oldPassword, String newPassword,
								SimpleLoginCompletionHandler handler) {
		
	}
		
	/**
	 * Legacy checkAuthStatus
	 * @param handler
	 */
	public void checkAuthStatus (SimpleLoginCompletionHandler handler) {
		
	}
	
}
