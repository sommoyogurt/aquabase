package com.tomting.aquabase.client.security;

public interface SimpleLoginCompletionHandler {
	public void completed(Error error, boolean success);
}
