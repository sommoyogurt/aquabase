package com.tomting.aquabase.client.security;

public interface SimpleLoginAuthenticatedHandler {
	public void authenticated(Error error, User user);
}
