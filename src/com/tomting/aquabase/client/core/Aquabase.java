package com.tomting.aquabase.client.core;

import java.util.Map;

import com.tomting.aquabase.client.persistence.Reference;
import com.tomting.aquabase.common.constants.AquabaseError;
import com.tomting.aquabase.common.constants.Constants;

public class Aquabase extends Query {

		
	public Aquabase (String reference) {
		this.reference = new Reference (reference);
	}
	
	public Aquabase (Reference reference) {
		this.reference = new Reference (reference);
	}	
	
	/**
	 * Legacy CompletionListener
	 *
	 */
	public interface CompletionListener {

		public void onComplete (AquabaseError error);
	}
	
	/**
	 * Legacy ServerValue enums
	 *
	 */
	public enum ServerValue {
		
		TIMESTAMP
	}
		
	/**
	 * Legacy child
	 * @param pathString
	 * @return
	 */
	public Aquabase child (String pathString) {
		
		return new Aquabase (reference.addChild(pathString));
	}
	
	/**
	 * Legacy getParent
	 * @return
	 */
	public Aquabase getParent () {
		
		return new Aquabase (reference.getParent());
	}
	
	/**
	 * override toString
	 */
	@Override
	public String toString () {
		
		return reference.getStringReference();
	}
	
	/**
	 * Helper function
	 * @return
	 */
	public Reference getReference () {
		
		return reference;
	}
	
	/**
	 * Legacy getRoot
	 * @return
	 */
	public Aquabase getRoot () {

		return new Aquabase (reference.getRoot());		
	}
	
	/**
	 * Legacy push
	 * @return
	 */
	public Aquabase push () {
		
		return new Aquabase (reference.push ());
	}
	
	/**
	 * Legacy getName
	 * @return
	 */
	public String getName () {
		
		return reference.getName();
	}
	
	/**
	 * Legacy setPriority
	 * @param priority
	 */
	public void setPriority (Object priority) {
		
		reference.setPriority (priority);
	}
		
	/**
	 * Legacy setValue
	 * @param object
	 */
	public void setValue (Object object) {
		
		pm.setValue(reference, object, null);
	}
	
	/**
	 * Legacy setValue
	 * @param object
	 * @param completionListener
	 */
	public void setValue (Object object, CompletionListener completionListener) {
		
		pm.setValue(reference, object, completionListener);
	}
	
	/**
	 * Legacy updateChildren
	 * @param children
	 */
	public void updateChildren (Map<String, Object> children) {
		
		pm.updateChildren (reference, children);
	}
	
	/**
	 * Legacy removeValue
	 */
	public void removeValue() {
		
		pm.removeValue (reference, null);
	}
	
	/**
	 * Legacy removeValue
	 * @param listener
	 */
	public void removeValue (CompletionListener listener) {
	
		pm.removeValue (reference, listener);		
	}
				
	/**
	 * Legacy onDisconnect
	 * @return
	 */
	public Aquabase onDisconnect () {
		
		//TODO
		return null;
	}
	
	/**
	 * Legacy remove used for onDisconnect
	 */
	public void remove () {
		
		//TODO
	}
	
	/**
	 * Legacy
	 * @return
	 */
	public static String getSdkVersion () {
		return Constants.version;
	}
	
	public static void goOnline () {
		pm.goOnline();
	}
	
	public static void goOffline () {
		pm.goOffline();
	}
	
}
