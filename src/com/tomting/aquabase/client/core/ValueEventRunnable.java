package com.tomting.aquabase.client.core;


public class ValueEventRunnable implements Runnable {
	public enum Action {
		ONDATACHANGE,
		ONCANCELLED
	}
	private ValueEventListener valueEventListener;
	private Action action;
	private DataSnapshot snapshot;

	
	public ValueEventRunnable (
			ValueEventListener valueEventListener, Action action, DataSnapshot snapshot) {
		
		this.valueEventListener = valueEventListener;
		this.action = action;
		this.snapshot = snapshot;

	}
	
	@Override
	public void run() {
		switch (action) {
		case ONDATACHANGE:
			valueEventListener.onDataChange(snapshot);		
		case ONCANCELLED:
			valueEventListener.onCancelled();
			break;

		}
		
	}

}
