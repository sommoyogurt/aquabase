package com.tomting.aquabase.client.core;

public interface ValueEventListener {
	
	public void onDataChange (DataSnapshot snapshot);
	public void onCancelled ();
}
