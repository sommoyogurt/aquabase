package com.tomting.aquabase.client.core;

public interface ChildEventListener {

	public void onChildAdded (DataSnapshot snapshot, String previousChildName);
	public void onChildChanged (DataSnapshot snapshot, String previousChildName);
	public void onChildRemoved (DataSnapshot snapshot);
	public void onChildMoved (DataSnapshot snapshot, String previousChildName);
	public void onCancelled ();
}
