package com.tomting.aquabase.client.core;


public class ChildEventRunnable implements Runnable {
	public enum Action {
		ONCHILDADDED,
		ONCHILDCHANGED,
		ONCHILDREMOVED,
		ONCANCELLED
	}
	private ChildEventListener childEventListener;
	private Action action;
	private DataSnapshot snapshot;
	private String previousChildName;
	
	public ChildEventRunnable (
			ChildEventListener childEventListener, Action action, DataSnapshot snapshot, String previousChildName) {
		
		this.childEventListener = childEventListener;
		this.action = action;
		this.snapshot = snapshot;
		this.previousChildName = previousChildName;
	}
	
	@Override
	public void run() {
		switch (action) {
			case ONCHILDADDED:
				childEventListener.onChildAdded(snapshot, previousChildName);
				break;
			case ONCHILDCHANGED:
				childEventListener.onChildChanged(snapshot, previousChildName);
				break;
			case ONCHILDREMOVED:
				childEventListener.onChildRemoved(snapshot);
				break;
			case ONCANCELLED:
				childEventListener.onCancelled();
				break;
		}
		
	}

}
