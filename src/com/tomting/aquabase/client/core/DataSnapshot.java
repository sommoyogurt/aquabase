package com.tomting.aquabase.client.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tomting.aquabase.client.persistence.ReferenceTree;

public class DataSnapshot {
	private Aquabase ref;
	private ReferenceTree node;
	private ObjectMapper jsonWorker = new ObjectMapper ();
	
	/**
	 * Legacy internal constructor
	 * @param ref
	 * @param node
	 */
	public DataSnapshot(Aquabase ref, ReferenceTree node) {
	
		this.ref = ref;
		this.node = node;
	}
	
	/**
	 * Legacy
	 * @param path
	 * @return
	 */
	public boolean hasChild(String path) {
		
		//TODO
		return false;
	}
	
	/**
	 * Legacy
	 * @return
	 */
	public boolean hasChildren() {
		
		//TODO
		return false;
	}
	
	/**
	 * Legacy
	 * @return
	 */
	public Object getValue() {
				
		return node.getValue();
	}
	
	/**
	 * Legacy
	 * @param useExportFormat
	 * @return
	 */
	public Object getValue(boolean useExportFormat) {
		
		//TODO
		return null;
	}
	
	/**
	 * TODO
	 * @param valueType
	 * @return
	 */
	public <T> T getValue(Class<T> valueType) {
		
		try {
			return jsonWorker.readValue(node.toJson(false), valueType);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Legacy
	 * @param t
	 * @return
	 */
	public <T> T getValue(GenericTypeIndicator<T> t) {
		
		try {		
			return jsonWorker.readValue(jsonWorker.writeValueAsString(node.getValue()), t);
		} catch (Exception e) {
			return null;
		}
	}
	
	public long getChildrenCount() {
		
		//TODO
		return 0;
	}
	
	/**
	 * Legacy
	 * @return
	 */
	public Aquabase getRef() {
		
		return ref;
	}
	
	/**
	 * Legacy
	 * @return
	 */
	public String getName() {
		
		return node.getKey();
	}
	
	/**
	 * Legacy
	 * @return
	 */
	public Iterable<DataSnapshot> getChildren() {
		
		//TODO
		return null;
	}
	
	/**
	 * Legacy
	 * @return
	 */
	public Object getPriority() {
		
		//TODO
		return null;		
	}
	
	/**
	 * Legacy child
	 * @param path
	 * @return
	 */
	public DataSnapshot child (String path)	{
					
		return new DataSnapshot (ref, node.getValueTree ().child(path));
	}
	
	/**
	 * Helpers: json print
	 */
    @Override
	public String toString () {
		
		return node.toJson(false);
	}
	
}
