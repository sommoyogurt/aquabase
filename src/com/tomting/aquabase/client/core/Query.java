package com.tomting.aquabase.client.core;

import com.tomting.aquabase.client.persistence.PersistenceManager;
import com.tomting.aquabase.client.persistence.PersistenceManagerFactory;
import com.tomting.aquabase.client.persistence.Reference;

public class Query {
	protected static final PersistenceManager pm = PersistenceManagerFactory.get();	
	protected Reference reference;	
	
	public Query () {
		
	}

	/**
	 * Legacy addListenerForSingleValueEven
	 * @param listener
	 */
	public void addListenerForSingleValueEvent (ValueEventListener listener) {
		
		//TODO
	}	
	
	/**
	 * Legacy addValueEventListener
	 * @param listener
	 */
	public void addValueEventListener (ValueEventListener listener) {
		
		pm.addValueEventListener(reference, listener);
	}	
	
	/**
	 * Legacy removeEventListener
	 * @param listener
	 */
	public void removeEventListener (ValueEventListener listener) {
		
		pm.removeEventListener (reference, listener);
	}
	
	/**
	 * Legacy removeEventListener
	 * @param listener
	 */
	public void removeEventListener (ChildEventListener listener) {
		
		pm.removeEventListener (reference, listener);
	}	
	
	/**
	 * legacy addChildEventListener
	 * @param listener
	 */
	public void addChildEventListener (ChildEventListener listener) {
		
		pm.addChildEventListener (reference, listener);
	}	
	
}
