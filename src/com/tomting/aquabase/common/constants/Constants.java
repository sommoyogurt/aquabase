package com.tomting.aquabase.common.constants;

import java.util.Properties;

import com.tomting.aquabase.common.helpers.Helpers;

public final class Constants {
	public static final String version = "1.0.01 (5 dec)";
	
	public static final String serverPropertyFileName = "/aquabaseserver.ini";
	public static final String clientPropertyFileName = "/aquabaseclient.ini";
	public static final String urlProperty = "orion";
	public static final String workersProperty = "workers";
	public static final String bulkSizeProperty = "bulkSize";
	public static final String serviceResultFalse = "Orion statement returned false";
	public static final String MainAquabaseTable = "AQUABASE";
	public static final String MainAquabaseValueColumn = "VALUE";
	public static final String thriftJavaHost = "thriftJavaHost";	
	public static final String thriftJavaPort = "thriftJavaPort";
	public static final String localhost = "localhost";	
	public static final String poolExpirationTime = "poolExpirationTime";
	
	public static final int defaultBufferSize = 10240;
	public static final String defaultDisposition = "inline";	
	public static final long defaultExpireTime = 604800000L;	
	public static final String encoding = "UTF-8";	
	
	public static final Properties serverProperties = Helpers.getProperties(serverPropertyFileName); 	
	public static final Properties clientProperties = Helpers.getProperties(clientPropertyFileName);  	
	
	private Constants () {}
	
	
	/**
	 * String property
	 * @param key
	 * @return
	 */
	public static String getServerStringProperty (String key) {
		return serverProperties.getProperty(key);
	}	
	
	/**
	 * Integer property
	 * @param key
	 * @return
	 */
	public static int getServerIntProperty (String key) {
		return Integer.valueOf(serverProperties.getProperty(key));
	}
	
	/**
	 * String property
	 * @param key
	 * @return
	 */
	public static String getClientStringProperty (String key) {
		return clientProperties.getProperty(key);
	}	
	
	/**
	 * Integer property
	 * @param key
	 * @return
	 */
	public static int getClientIntProperty (String key) {
		return Integer.valueOf(clientProperties.getProperty(key));
	}	
}
