package com.tomting.aquabase.common.helpers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SimpleSerializer {

	/**
	 * deserialization of a String back to Object
	 * @param s
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Object deserialize (String s)  {
        byte [] data = Base64Coder.decode(s);
        try {
	        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
	        Object o = ois.readObject();
	        ois.close();
	        return o;
        } catch (Exception o) {
        	return null;
        }
    }

    /**
     * serialization of Serializable Object to String
     * @param o
     * @return
     * @throws IOException
     */
    public static String serialize (Serializable o) {
    	
    	try{
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        ObjectOutputStream oos = new ObjectOutputStream( baos );
	        oos.writeObject (o);
	        oos.close();
	        return new String(Base64Coder.encode( baos.toByteArray()));
    	} catch (Exception e) {
    		return null;
    	}
    }	
	
}
