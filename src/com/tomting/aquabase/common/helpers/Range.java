package com.tomting.aquabase.common.helpers;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Range {
    public long start;
    public long end;
    public long length;
    public long total;
    public int partition;

    public Range(long start, long end, long total, int partition) {
        this.start = start;
        this.end = end;
        this.length = end - start + 1;
        this.total = total;
        this.partition = partition;
    }
        
    public static void order (List<Range> rangeBarrel) {
    	
	    Collections.sort(rangeBarrel, new Comparator<Range>() {
	        public int compare(Range a, Range b) {
	            return Long.signum(a.start - b.start);
	        }
	    }); 
    }
    
    
}	
