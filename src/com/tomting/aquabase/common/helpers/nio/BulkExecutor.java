package com.tomting.aquabase.common.helpers.nio;

import java.util.List;

public abstract class BulkExecutor<	GenericService, GenericServiceResult,
									GenericJob extends Job<	GenericService, GenericServiceResult>> {

	public abstract List<GenericServiceResult> executeSync (List<GenericService> bulkInput);
	public abstract void executeAsync (List<GenericService> bulkInput, List<GenericJob> bulkCallback);
}
