package com.tomting.aquabase.common.helpers.nio;

import java.util.ArrayList;
import java.util.List;

public class WorkerPool<	GenericService, GenericServiceResult,
							GenericJob extends Job<	GenericService, GenericServiceResult>>  {
	private boolean async;
	private BulkExecutor<GenericService, GenericServiceResult, GenericJob> be;
	private final Class<GenericJob> type;	
	private List<Worker<GenericService, GenericServiceResult,GenericJob>> workerPool = 
		new ArrayList<Worker<GenericService, GenericServiceResult,GenericJob>> ();
	private int workers;
	private int bulkSize;
	private int simpleRule = 0;
	private int subCounter = 0;	
	private int refCount = 0;

	public WorkerPool (	boolean async, Class<GenericJob> type, 
						BulkExecutor<GenericService, GenericServiceResult, GenericJob> be, 
						int workers, int bulkSize) {
		
		this.async = async;
		this.workers = workers;
		this.bulkSize = bulkSize;
		this.type = type;
		this.be = be;
	}
	
	/**
	 * Add reference
	 * @return
	 */
	public WorkerPool<GenericService, GenericServiceResult,GenericJob> addRef () {
		
		synchronized (this) {
			refCount++;
			if (refCount == 1) {
				for (int i = 0; i < workers; i++) {
					Worker<GenericService, GenericServiceResult,GenericJob> worker = 
							new Worker<GenericService, GenericServiceResult,GenericJob> (async, type, be, bulkSize);
					worker.setDaemon(true);
					worker.start();
					workerPool.add(worker);
				}				
			}
		}
		return this;
	}
	
	/**
	 * Remove reference
	 * @return
	 */
	public WorkerPool<GenericService, GenericServiceResult,GenericJob> subRef () {
		
		synchronized (this) {
			refCount--;
			if (refCount == 0) {
				for (Worker<GenericService, GenericServiceResult,GenericJob> w : workerPool) {
					try {
						w.abort();
						w.join();
					} catch (InterruptedException e) {}			
				}				
			}
		}
		return this;
	}	
	
	/**
	 * Add job
	 * @param job
	 * @throws InterruptedException
	 */
	public void addJob (GenericJob job) throws InterruptedException {
		int index;
		
		synchronized (this) {
			subCounter = ++subCounter % bulkSize;	
			if (subCounter == 0) index = ++simpleRule % workers;
			else index = simpleRule % workers;
		}
		workerPool.get(index).addJob(job);
	}	
		
}
