package com.tomting.aquabase.common.helpers.nio;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Worker<	GenericService, GenericServiceResult,
						GenericJob extends Job<	GenericService, GenericServiceResult>> extends Thread {
	private boolean async;
	private BlockingQueue<GenericJob> queue = new LinkedBlockingQueue<GenericJob> ();
	private int bulkSize;
	private BulkExecutor<GenericService, GenericServiceResult, GenericJob> be;
	private final Class<GenericJob> type;
	
    GenericJob createContents(Class<GenericJob> classOfT) throws InstantiationException, IllegalAccessException {
        return classOfT.newInstance();
    }	
	
	public Worker (	boolean async,
					Class<GenericJob> type, 
					BulkExecutor<GenericService, GenericServiceResult, GenericJob> be, int bulkSize) {
		this.async = async;
		this.type = type;
		this.be = be;
		this.bulkSize = bulkSize;
	}
	
	public void addJob (GenericJob job) throws InterruptedException {
		
		queue.put(job);
	}	
	
	public void abort ()  {
	
		try {
			queue.add(createContents (type));
		} catch (InstantiationException e) {
		} catch (IllegalAccessException e) {
		}
	}	
	
	@Override
	public void run () {
	
		while (true) {
			try {
				List<GenericJob> bulk = new ArrayList<GenericJob> ();
				List<GenericService> batch = new ArrayList<GenericService> ();
				GenericJob job = queue.take();
				if (job.isValid()) {
					bulk.add(job);
					batch.add(job.getService());
				}				
				int bulkDim = Math.min(queue.size(), bulkSize);
				while (job.isValid() && bulkDim-- > 0) {
					job = queue.take();
					if (job.isValid()) {
						bulk.add(job);
						batch.add(job.getService());
					}
				}
				if (batch.size() > 0) {
					if (!async) {
						List<GenericServiceResult> batchResult = be.executeSync(batch);					
				        for (int i = 0; i < bulk.size();i++) {
				        	try {
				        		bulk.get(i).setResult(batchResult.get(i));
				        	} catch (Exception e) {
				        		bulk.get(i).setResult(e.getMessage());
				        	}
				        	bulk.get(i).syncReleaseLock();
				        }	
					} else {
						be.executeAsync(batch, bulk);
					}
				}
				if (!job.isValid()) break;
			} catch (InterruptedException e) {}
			
		}
	}	
	
}
