package com.tomting.aquabase.common.helpers.nio;

public abstract class Job<GenericService, GenericServiceResult> {

	public Job () {}
	public abstract Job<GenericService, GenericServiceResult> create ();
	public abstract boolean isValid ();
	public abstract GenericService getService ();
	public abstract void setResult (GenericServiceResult result);
	public abstract void setResult (String error);	
	public abstract void syncReleaseLock ();
	public abstract void syncWaitLock ();
}
