package com.tomting.aquabase.common.helpers;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

public final class Helpers {

	/**
	 * Get properties from file or stream
	 * @param name
	 * @return
	 */
	@SuppressWarnings("resource")
	public static Properties getProperties (String name) {
        InputStream is = null;		
        try {
        	File f = new File(name);
        	is = new FileInputStream (f);
        } catch (Exception e) {
        }
        if (is == null) is = Helpers.class.getResourceAsStream(name);	
        try {
        	Properties props = new Properties(); 
	        props.load (is);
	        is.close(); 
	        return props;
        } catch (Exception e) {
        	return null;
        }   
	}	
	
	/**
	 * Print HTTP header
	 * @param req
	 * @return
	 */
	public static String printHeader (HttpServletRequest req) {
		String result = "";
		
		Enumeration<String> en = req.getHeaderNames();
	    while (en.hasMoreElements()) {
	        String headers = en.nextElement();
	        if (headers != null) result += headers + " - " + req.getHeader(headers) + "\n";
	    }			
	    return result;
	}	
	
	/**
	 * Accepts HTTP header
	 * @param acceptHeader
	 * @param toAccept
	 * @return
	 */
    public static boolean accepts (String acceptHeader, String toAccept) {
        String[] acceptValues = acceptHeader.split("\\s*(,|;)\\s*");
        Arrays.sort(acceptValues);
        return Arrays.binarySearch(acceptValues, toAccept) > -1
            || Arrays.binarySearch(acceptValues, toAccept.replaceAll("/.*$", "/*")) > -1
            || Arrays.binarySearch(acceptValues, "*/*") > -1;
    }  
    
	/**
	 * Matches HTTP header
	 * @param matchHeader
	 * @param toMatch
	 * @return
	 */
    public static boolean matches (String matchHeader, String toMatch) {
    	
        String[] matchValues = matchHeader.split("\\s*,\\s*");
        Arrays.sort(matchValues);
        return Arrays.binarySearch(matchValues, toMatch) > -1
            || Arrays.binarySearch(matchValues, "*") > -1;
    }	    
    
	/**
	 * ReadStream
	 * @param input
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public static String readStream (HttpServletRequest input)
		throws ServletException, IOException{
		
		BufferedReader reader = input.getReader();        
    	StringBuilder sb = new StringBuilder();
    	String line = reader.readLine();
    	while (line != null) {		
    		sb.append(line + "\n");
    		line = reader.readLine();
    	}
    	reader.close();        			
    	return sb.toString();
	}  
	
	/**
	 * Range (HTTP)
	 * @param value
	 * @param beginIndex
	 * @param endIndex
	 * @return
	 */
    private static long sublong(String value, int beginIndex, int endIndex) {
        String substring = value.substring(beginIndex, endIndex);
        return (substring.length() > 0) ? Long.parseLong(substring) : -1;
    }		
	
	/**
	 * Range (HTTP)
	 * @param string
	 * @param length
	 * @param rangeBarrel
	 * @return
	 */
	public static boolean fillRangeBarrel (String string, long length, List<Range> rangeBarrel) {
	
		if (!string.matches("^bytes=\\d*-\\d*(,\\d*-\\d*)*$")) return false;
		rangeBarrel.clear();
        for (String part : string.substring(6).split(",")) {
            long start = sublong(part, 0, part.indexOf("-"));
            long end = sublong(part, part.indexOf("-") + 1, part.length());

            if (start == -1) {
                start = length - end;
                end = length - 1;
            } else if (end == -1 || end >= length) {
                end = length - 1;
            }
            if (start > end) return false;
            rangeBarrel.add(new Range(start, end, length, -1));
        }
		return true;
	}	

	/**
	 * Range (HTTP)	
	 * @param rangeBarrel
	 * @param partitionBarrel
	 * @return
	 */
	public static List<Range> getRangeBarrel (List<Range> rangeBarrel, List<Range> partitionBarrel) {
		List<Range> result = new ArrayList<Range> ();
		
		if (rangeBarrel.size() > 0) {
			Range r = rangeBarrel.get(0);
			rangeBarrel.remove(0);
			for (Range p : partitionBarrel) {			
				if ((p.start <= r.end && p.start >= r.start) || (p.end >= r.start && p.end <= r.end) || 
					(r.start <= p.end && r.start >= p.start) || (r.end >= p.start && r.end <= p.end)) {					
					result.add (
							new Range (Math.max(p.start, r.start) - p.start, Math.min (p.end, r.end) - p.start, -1, p.partition));
				}
				
			}
		}
		return result;
	}	
	
	/**
	 * Range (HTTP)	
	 * @param partitionDims
	 * @param length
	 * @return
	 */
	public static List<Range> getPartitionBarrel (List<Integer> partitionDims, long length) {
		int position = 0;
		int counter = 0;
		List<Range> result = new ArrayList<Range> ();
		
		for (Integer i : partitionDims) {
			result.add(new Range (position, position + i - 1, length, counter++));
			position += i;
		}
		return result;
	}	
	
}
