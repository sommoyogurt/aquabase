package com.tomting.aquabase.common.thrift;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.async.TAsyncClientManager;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TBinaryProtocol.Factory;
import org.apache.thrift.transport.TNonblockingSocket;

import com.tomting.aquabase.ThriftAquabase;
import com.tomting.aquabase.ThriftAquabase.AsyncClient;
import com.tomting.aquabase.ThriftKeyValuePair;
import com.tomting.aquabase.ThriftUserNamespace;
import com.tomting.aquabase.ThriftWrite;
import com.tomting.aquabase.ThriftWriteResult;

public final class Helpers {
	private Helpers () {}
	
	/**
	 * Bulk write result template
	 * @param bulkSize
	 * @return
	 */
	public static List<ThriftWriteResult> getBulkWriteResult (int bulkSize) {
		
		List<ThriftWriteResult> result = new ArrayList<ThriftWriteResult> ();
		for (int i = 0; i < bulkSize; i++) result.add(new ThriftWriteResult ().setResult(false));
		return result;
	}
		
	public static ThriftWrite getWrite (String namespace, String key, String value) {
		ThriftWrite result = new ThriftWrite ();
		result.userNamespace = new ThriftUserNamespace ();
		result.userNamespace.setUserNamespace(namespace);
		result.keyValuePair = new ThriftKeyValuePair ();
		result.keyValuePair.setKey(key);
		result.keyValuePair.setValue(value);
		return result;
	}
	
	/**
	 * Static async client from a socket
	 * @param socket
	 * @return
	 * @throws IOException
	 */
	public static AsyncClient getAsyncClient (TNonblockingSocket socket) throws IOException {
		
		Factory binaryProtocol = new TBinaryProtocol.Factory();
		TAsyncClientManager clientManager = new TAsyncClientManager ();		
		return new ThriftAquabase.AsyncClient (binaryProtocol, clientManager, socket);		
	}
		
}
