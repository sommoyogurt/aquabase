package com.tomting.aquabase.server.orion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import com.tomting.aquabase.ThriftKeyValuePair;
import com.tomting.aquabase.ThriftReadResult;
import com.tomting.aquabase.client.persistence.Reference;
import com.tomting.aquabase.common.constants.AquabaseError;
import com.tomting.aquabase.common.constants.Constants;
import com.tomting.aquabase.common.helpers.nio.Job;
import com.tomting.orion.ThrfL2cl;
import com.tomting.orion.ThrfL2cv;
import com.tomting.orion.ThrfL2ks;
import com.tomting.orion.ThrfLkey;
import com.tomting.orion.ThrfSrvc;
import com.tomting.orion.ThrfSrvr;
import com.tomting.orion.iEcolumntype;
import com.tomting.orion.iEopcodetype;
import com.tomting.orion.iEquerytype;
import com.tomting.orion.iEservicetype;
import com.tomting.orion.iEstatetype;
import com.tomting.orion.connection.OrionConnection;

public class NioServiceTransport extends Job<ThrfSrvc, ThrfSrvr> {	
	private String namespace;
	private String key;
	private String value;
	private Action serviceAction;
	private CompletionListener listener;
	private boolean valid;	
	private Semaphore lock;
	
	public enum Action {
		WRITE,
		READ,
		DELETE
	}
	
	/**
	 * Poisoned atom
	 */
	public NioServiceTransport () {
		
		valid = false;
	}	
	
	private void init (String namespace, String key, String value, Action serviceAction, CompletionListener listener) {
	
		this.namespace = namespace;
		this.key = key;
		this.value = value;
		this.serviceAction = serviceAction;
		this.listener = listener;
		valid = true;
		lock = new Semaphore (0);
	}
	
	/**
	 * Atom
	 * @param key
	 * @param o
	 * @param listener
	 * @throws IOException
	 */
	public NioServiceTransport (String namespace, String key, String value, Action serviceAction, CompletionListener listener)  {
	
		init (namespace, key, value, serviceAction, listener);
	}
	
	public NioServiceTransport (String namespace, String key, Action serviceAction, CompletionListener listener)  {
		
		init (namespace, key, null, serviceAction, listener);
	}	
	
	/**
	 * Check atom is not poisoned
	 * @return
	 */
	public boolean isValid () {
		return valid;
	}
	
	/**
	 * Return Orion Service
	 * @return
	 */
	public ThrfSrvc getService () {
		ThrfSrvc service = new ThrfSrvc ();
		ThrfL2cl cVcolumn = new ThrfL2cl ();
		cVcolumn.sVcolumn = Constants.MainAquabaseValueColumn;
		cVcolumn.cVvalue = new ThrfL2cv ();
		cVcolumn.iVtype = cVcolumn.cVvalue.iVtype = iEcolumntype.STRINGTYPE;			
		
		switch (serviceAction) {
			case DELETE:
			case WRITE:
				service.cVstatement = OrionConnection.getStatement();
				service.cVstatement.cVmutable.sVnamespace = namespace;	
				service.cVstatement.cVmutable.sVtable = Constants.MainAquabaseTable;
				service.cVstatement.cVkey.sVmain = key;
				service.cVstatement.cVkey.iVtimestamp = com.tomting.orion.connection.Helpers.getTimestamp();
				service.cVstatement.cVkey.iVstate = (serviceAction == Action.WRITE ? iEstatetype.UPSERT : iEstatetype.DELTMB);
				service.cVstatement.iVopcode = iEopcodetype.MUTATOR;
				cVcolumn.cVvalue.sVvalue = value;
				service.cVstatement.cVcolumns.add(cVcolumn);
				service.iVservicetype = iEservicetype.STATEMENT;			
				break;
			case READ:
				service.cVquery = OrionConnection.getQuery();
				service.cVquery.cVmutable.sVnamespace = namespace;	
				service.cVquery.cVmutable.sVtable = Constants.MainAquabaseTable;
				service.cVquery.cVkey_start = new ThrfLkey ();
				service.cVquery.cVkey_start.sVmain = key;
				service.cVquery.cVkey_end = new ThrfLkey ();
				service.cVquery.cVkey_end.sVmain = Reference.ceilKey(key);
				service.cVquery.iVquery = iEquerytype.RANGEQUERY;
				service.cVquery.bVonlysecondary = false;
				service.cVquery.cVselect.add(cVcolumn);
				service.iVservicetype = iEservicetype.QUERY;
				break;
			default:
		}
		return service;
	}

	@Override
	public void setResult(String error) {
		if (listener != null) listener.onComplete(new AquabaseError (error));	
	}

	@Override
	public void setResult(ThrfSrvr result) {
		if (listener != null) {
			switch (serviceAction) {
				case WRITE:	
					listener.onComplete(result.bVreturn ? null : new AquabaseError (Constants.serviceResultFalse));
					break;
				case READ:
					if (result.cVqueryresult.bVreturn) {
						ThriftReadResult readResult = new ThriftReadResult ();
						readResult.setResult(true);
						readResult.keyValuePair = new ArrayList<ThriftKeyValuePair> ();
						for ( ThrfL2ks k: result.cVqueryresult.cKeyslices) {
							ThriftKeyValuePair keyValuePair = new ThriftKeyValuePair ();
							keyValuePair.setKey(k.cVkey.sVmain);
							keyValuePair.setValue(k.cVcolumns.get(0).cVvalue.sVvalue);
							readResult.keyValuePair.add(keyValuePair);
						}						
						listener.onComplete(null, readResult);
					} else 
						listener.onComplete(new AquabaseError (Constants.serviceResultFalse), null);
					break;
			default:
				break;
			}
		}
	}

	@Override
	public Job<ThrfSrvc, ThrfSrvr> create() {

		return new NioServiceTransport (); 
	}

	@Override
	public void syncReleaseLock() {
		lock.release();
	}

	@Override
	public void syncWaitLock()  {
		try {
			lock.acquire();
		} catch (InterruptedException e) {
		}
		
	}


	
}
