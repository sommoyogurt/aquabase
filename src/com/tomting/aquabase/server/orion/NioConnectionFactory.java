package com.tomting.aquabase.server.orion;

import com.tomting.aquabase.common.constants.Constants;
import com.tomting.aquabase.common.helpers.nio.WorkerPool;
import com.tomting.orion.ThrfSrvc;
import com.tomting.orion.ThrfSrvr;
import com.tomting.orion.connection.Helpers;


public class NioConnectionFactory {	
	private static final WorkerPool<ThrfSrvc, ThrfSrvr, NioServiceTransport> singleton = 
			new WorkerPool<ThrfSrvc, ThrfSrvr, NioServiceTransport> (
					false, NioServiceTransport.class, 
					new NioBulkExecutor (Helpers.getOCF (Constants.getServerStringProperty(Constants.urlProperty))),
					Constants.getServerIntProperty(Constants.workersProperty),
					Constants.getServerIntProperty(Constants.bulkSizeProperty)).addRef();
	
	private NioConnectionFactory () {}
	
	public static WorkerPool<ThrfSrvc, ThrfSrvr, NioServiceTransport> get () {
		return singleton;
	}
}
