package com.tomting.aquabase.server.orion;

import java.util.List;

import org.apache.thrift.TException;

import com.tomting.aquabase.common.helpers.nio.BulkExecutor;
import com.tomting.orion.ThrfSrvc;
import com.tomting.orion.ThrfSrvr;
import com.tomting.orion.connection.OCI;
import com.tomting.orion.connection.OrionConnection;
import com.tomting.orion.connection.OrionConnectionFactory;

public class NioBulkExecutor extends BulkExecutor<ThrfSrvc, ThrfSrvr, NioServiceTransport> {
	OrionConnectionFactory ocf;
	
	public NioBulkExecutor (OrionConnectionFactory ocf) {
		
		this.ocf = ocf;
	}
	
	@Override
	public List<ThrfSrvr> executeSync (List<ThrfSrvc> bulkInput) {
		List<ThrfSrvr> bulkOutput = null;
 
		OCI orion = ocf.checkOut();        
        try {
        	bulkOutput = ((OrionConnection) orion).runThriftbatch(bulkInput);  

        	

        	
        	
		} catch (TException e) {	
			e.printStackTrace();
		} finally {
			ocf.checkIn(orion);
		} 
        return bulkOutput;
	}

	/**
	 * Not used
	 */
	@Override
	public void executeAsync(List<ThrfSrvc> bulkInput,
			List<NioServiceTransport> bulkCallback) {		
	}

}
