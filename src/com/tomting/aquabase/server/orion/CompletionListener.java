package com.tomting.aquabase.server.orion;

import com.tomting.aquabase.ThriftReadResult;
import com.tomting.aquabase.common.constants.AquabaseError;

public interface CompletionListener {
	public void onComplete (AquabaseError error);
	public void onComplete (AquabaseError error, ThriftReadResult readResult);
}
