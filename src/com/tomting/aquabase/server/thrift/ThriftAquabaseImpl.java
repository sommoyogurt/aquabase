package com.tomting.aquabase.server.thrift;

import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.TException;

import com.tomting.aquabase.ThriftAquabase;
import com.tomting.aquabase.ThriftWrite;
import com.tomting.aquabase.ThriftWriteResult;
import com.tomting.aquabase.common.helpers.nio.WorkerPool;
import com.tomting.aquabase.common.thrift.Helpers;
import com.tomting.aquabase.server.orion.NioConnectionFactory;
import com.tomting.aquabase.server.orion.NioServiceTransport;
import com.tomting.orion.ThrfSrvc;
import com.tomting.orion.ThrfSrvr;



public class ThriftAquabaseImpl implements ThriftAquabase.Iface {
	WorkerPool<ThrfSrvc, ThrfSrvr, NioServiceTransport> connection = NioConnectionFactory.get();
	
	@Override
	public boolean ping() throws TException {

		System.out.println ("Ping");
		return true;
	}

	@Override
	public List<ThriftWriteResult> setValue(List<ThriftWrite> bulkWrite)
			throws TException {

		List<ThriftWriteResult> result = Helpers.getBulkWriteResult (bulkWrite.size());
		List<NioServiceTransport> transportPool = new ArrayList <NioServiceTransport> ();
		for (int i = 0; i < bulkWrite.size(); i++) {
			ThriftWrite w = bulkWrite.get(i);
			
			try {
				NioServiceTransport transport = 
						new NioServiceTransport (	w.userNamespace.userNamespace.toUpperCase(), 
													w.keyValuePair.key, 
													w.keyValuePair.value, NioServiceTransport.Action.WRITE, 
													new SetValueCompletionListener (result.get(i)));
				transportPool.add(transport);
				connection.addJob(transport);
			} catch (Exception e) {}
		}
		for ( NioServiceTransport t : transportPool) t.syncWaitLock();
		return result;	
	}
}


