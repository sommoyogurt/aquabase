package com.tomting.aquabase.server.thrift;

import org.apache.thrift.server.TNonblockingServer;
import org.apache.thrift.server.TServer;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TNonblockingServerTransport;
import org.apache.thrift.transport.TTransportException;
import com.tomting.aquabase.common.constants.Constants;

import com.tomting.aquabase.ThriftAquabase;

public class Aquaserver {

    private void start() {
        try {
        	int port = Constants.getServerIntProperty(Constants.thriftJavaPort);
            TNonblockingServerTransport serverTransport = new TNonblockingServerSocket(port);
			ThriftAquabase.Processor<ThriftAquabaseImpl> processor = 
				new ThriftAquabase.Processor<ThriftAquabaseImpl>(new ThriftAquabaseImpl());

            TServer server = new TNonblockingServer(new TNonblockingServer.Args(serverTransport).
                    processor(processor));
            System.out.println("Aquabase: " + Constants.version);
            System.out.println("NIO on localhost:" + port);
            server.serve();
        } catch (TTransportException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
    	Aquaserver srv = new Aquaserver ();
        srv.start();
    }

	
}
