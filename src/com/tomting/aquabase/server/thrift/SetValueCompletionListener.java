package com.tomting.aquabase.server.thrift;

import com.tomting.aquabase.ThriftReadResult;
import com.tomting.aquabase.ThriftWriteResult;
import com.tomting.aquabase.common.constants.AquabaseError;
import com.tomting.aquabase.server.orion.CompletionListener;

public class SetValueCompletionListener implements CompletionListener {
	ThriftWriteResult writeResult;
	
	public SetValueCompletionListener (ThriftWriteResult writeResult) {
		this.writeResult = writeResult;
	}
	
	@Override
	public void onComplete(AquabaseError error) {
		writeResult.setResult(error == null);
		
	}

	/**
	 * Not used
	 */
	@Override
	public void onComplete(AquabaseError error, ThriftReadResult readResult) {		
	}
	
	
}