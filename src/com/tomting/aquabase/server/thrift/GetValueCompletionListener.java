package com.tomting.aquabase.server.thrift;

import com.tomting.aquabase.ThriftReadResult;
import com.tomting.aquabase.common.constants.AquabaseError;
import com.tomting.aquabase.server.orion.CompletionListener;

public class GetValueCompletionListener implements CompletionListener {
	ThriftReadResult readResult;

	public GetValueCompletionListener (ThriftReadResult readResult) {
		this.readResult = readResult;
	}

	/**
	 * Not used
	 */	
	@Override
	public void onComplete(AquabaseError error) {		
	}

	@Override
	public void onComplete(AquabaseError error, ThriftReadResult readResult) {
		this.readResult.setResult(error == null);
		if (error == null) {
			this.readResult.keyValuePair =  readResult.keyValuePair;
		};
		
	}
	
	
}