package com.tomting.aquabase.server.servlet;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;


public class GetValueUrl extends HttpServlet  {
	private static final long serialVersionUID = -147742443652265519L;
	private static final String PATCH = "PATCH";

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
	    if (request.getMethod().equals(PATCH))
	        doPatch(request, response);
	    else
	        super.service(request, response);
	}	
 
	public void doGet (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {	
		
		new ServletHandler(ServletImplementation.CallbackValue.GETVALUE, req, res, ServletHandler.HttpMethod.GET);
    }
    
    public void doPost (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException { 
    	
    	new ServletHandler (ServletImplementation.CallbackValue.GETVALUE, req, res, ServletHandler.HttpMethod.POST);
    }	  
    
    public void doPut (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException { 	
    	
    	new ServletHandler (ServletImplementation.CallbackValue.GETVALUE, req, res, ServletHandler.HttpMethod.PUT);
   }
    
    public void doDelete (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException { 	
    	
    	new ServletHandler (ServletImplementation.CallbackValue.GETVALUE, req, res, ServletHandler.HttpMethod.DELETE);
   }  
    
    public void doPatch (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException { 	
    	
    	new ServletHandler (ServletImplementation.CallbackValue.GETVALUE, req, res, ServletHandler.HttpMethod.PATCH);
   }       
}