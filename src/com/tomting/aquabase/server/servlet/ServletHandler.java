package com.tomting.aquabase.server.servlet;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.tomting.aquabase.common.constants.Constants;
import com.tomting.aquabase.common.helpers.Helpers;
import com.tomting.aquabase.common.helpers.Range;
import com.tomting.aquabase.server.servlet.ServletHandlerData;

public class ServletHandler {
	
	private final static Logger LOGGER = Logger.getLogger(ServletHandler.class.getName());  
	private final static String CONTENTDISPOSITION = "Content-Disposition";
	private final static String ETAG = "ETag";	
	private final static String LASTMODIFIED = "Last-Modified";		
	private final static String EXPIRES = "Expires";		
	private final static String ACCEPTRANGES = "Accept-Ranges";	
	private final static String BYTES = "bytes";	
	private final static String RANGE = "range";	
	private final static String CONTENTRANGE = "Content-Range";
	private final static String CONTENTLENGTH = "Content-Length";	
	private final static String IFRANGE = "If-Range";
	private final static String IFNONEMATCH = "If-None-Match";
	private final static String IFMODIFIEDSINCE = "If-Modified-Since";
	private final static String IFMATCH = "If-Match";	
	private final static String IFUNMODIFIEDSINCE = "If-Unmodified-Since";		
	private final static String ACCEPTENCODING = "Accept-Encoding";
	private final static String GZIP = "gzip";
	private final static String CONTENTENCODING = "Content-Encoding";	
	private final static String TEXTPLAIN = "text/plain";	
	
	private final static String JSON = "json";
	private final static String CALLBACK = "callback";	
	private final static String PRINT = "print";		
	
	private final static long oneSecond = 1000;
	
	public enum HttpMethod {
		GET,
		POST,
		PUT,
		DELETE,
		PATCH
	}
	
	public ServletHandler (		
			ServletImplementation.CallbackValue callbackFunction,
			HttpServletRequest req, 
			HttpServletResponse res,
			HttpMethod method) {

		ServletHandlerConstructor (callbackFunction, req, res, method);
	}	
		
	public void ServletHandlerConstructor (		
										ServletImplementation.CallbackValue callbackFunction,
										HttpServletRequest req, 
										HttpServletResponse res,
										HttpMethod method)  {
		
		ServletHandlerData callbackData = new ServletHandlerData ();
		String userCommand = null;
		String repliedToCommand = null;
		
		callbackData.setHttpMethod(method);
		try {	
			switch (method) {
				case GET:
					callbackData.setPath(req.getPathInfo());				
					callbackData.setInput(req.getParameter(JSON));
					callbackData.setCallback(req.getParameter(CALLBACK));	
					callbackData.setPrint(req.getParameter(PRINT));	
					String headerAcceptEncoding = req.getHeader(ACCEPTENCODING);
					callbackData.setEncodingGzip(headerAcceptEncoding != null && Helpers.accepts(headerAcceptEncoding, GZIP));					
					break;
				case POST:
					callbackData.setInput(Helpers.readStream (req));	
					break;
				case PUT:	
					callbackData.setPath(req.getPathInfo());					
					callbackData.setInput(Helpers.readStream (req));	
					break;	
				case DELETE:
					callbackData.setPath(req.getPathInfo());						
					callbackData.setInput(Helpers.readStream (req));
					break;
				case PATCH:
					callbackData.setPath(req.getPathInfo());						
					callbackData.setInput(Helpers.readStream (req));
					break;
			default:
				break;
			}
							
			String logInput = callbackData.getInput() != null ? "json:" + callbackData.getInput() : "path:" + callbackData.getPath();			
			userCommand = "Single - " + callbackFunction.name() + " - " +logInput;
									
			new ServletImplementation ().run(callbackFunction, callbackData); 			
		} catch (Exception e) {		
		    StringWriter sw = new StringWriter();
		    PrintWriter pw = new PrintWriter(sw);
		    e.printStackTrace(pw);		
		    String error = sw.toString();
		    
		    LOGGER.info(userCommand);
		    LOGGER.fatal(error);
		    								
		} finally {
			callbackData.setOutput();
			repliedToCommand = callbackData.getOutput() + "\n";
						
			try {
				if (callbackData.getMediaContentType() != null) {
					
					OutputStream outputStream = res.getOutputStream();					
					res.reset();
					res.setBufferSize(Constants.defaultBufferSize);	
					res.setHeader (CONTENTDISPOSITION, 
							Constants.defaultDisposition + ";filename=\"" + callbackData.getFilename() + "\"");					
					res.setHeader (ETAG, callbackData.geteTag());
					res.setDateHeader (LASTMODIFIED, callbackData.getLastModified());
					res.setDateHeader (EXPIRES, System.currentTimeMillis() + Constants.defaultExpireTime);						
					res.setContentType(callbackData.getMediaContentType());
			        String headerIfNoneMatch = req.getHeader(IFNONEMATCH);
			        long headerIfModifiedSince = req.getDateHeader(IFMODIFIEDSINCE);
			        String headerIfMatch = req.getHeader(IFMATCH);
			        long headerIfUnmodifiedSince = req.getDateHeader(IFUNMODIFIEDSINCE);
			        if ((headerIfNoneMatch != null && Helpers.matches(headerIfNoneMatch, callbackData.geteTag())) ||
			        	(headerIfNoneMatch == null && headerIfModifiedSince != -1 && 
			        		headerIfModifiedSince + oneSecond > callbackData.getLastModified())) {
			            res.setHeader(ETAG, callbackData.geteTag()); 
			            res.sendError(HttpServletResponse.SC_NOT_MODIFIED);
			        } else if ((headerIfMatch != null && !Helpers.matches(headerIfMatch, callbackData.geteTag())) ||
			        		 	(headerIfUnmodifiedSince != -1 && 
			        		 		headerIfUnmodifiedSince + oneSecond <= callbackData.getLastModified())) { 
			            res.sendError(HttpServletResponse.SC_PRECONDITION_FAILED);			        
			        } else {	
						int currentPartition = 0;
						List<Range> rangeBarrel = new ArrayList<Range> ();					
						res.setHeader(ACCEPTRANGES, BYTES);
						String ranges = req.getHeader(RANGE);
						boolean headerInputRanges = ranges != null;
						if (headerInputRanges && !Helpers.fillRangeBarrel(ranges, callbackData.getLength(), rangeBarrel)) {
							res.setHeader(CONTENTRANGE, "bytes */" + callbackData.getLength());
							res.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
						} else {
							Range full = new Range(0, callbackData.getLength() - 1, callbackData.getLength(), -1);
							String headerIfRange = req.getHeader(IFRANGE);			            
				            if (headerIfRange != null && !headerIfRange.equals(callbackData.geteTag())) {
				                try {
				                    long ifRangeTime = req.getDateHeader(IFRANGE); 
				                    if (ifRangeTime != -1 && ifRangeTime + oneSecond < callbackData.getLastModified()) 
				                    	rangeBarrel.add(full);
				                } catch (IllegalArgumentException ignore) {
				                    rangeBarrel.add(full);
				                }
				            }							
							if (rangeBarrel.isEmpty() || rangeBarrel.get(0) == full) {
					        	Range range = full;
					        	String contentRange = BYTES + " " + 
					            		range.start + "-" + range.end + "/" + range.total;
					            res.setHeader(CONTENTRANGE, contentRange);
					            res.setHeader(CONTENTLENGTH, String.valueOf(range.length));
					            if (rangeBarrel.isEmpty()) rangeBarrel.add(range);
					            res.setStatus(HttpServletResponse.SC_OK);	
					           
					            LOGGER.info(callbackData.getPath() + " > SC_OK, " + contentRange);
	    				            
					        } else if (rangeBarrel.size() == 1) {
					        	Range range = rangeBarrel.get(0);
					        	String contentRange = BYTES + " " + 
					            		range.start + "-" + range.end + "/" + range.total;
					            res.setHeader(CONTENTRANGE, contentRange);
					        	res.setHeader(CONTENTLENGTH, String.valueOf(range.length));
					        	res.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);	
					        	
					        	LOGGER.info(callbackData.getPath() + " > SC_PARTIAL_CONTENT, " + contentRange + " " + range.length);
					        	
					        } else {
					        	Range.order(rangeBarrel);
					        						        	
					        	LOGGER.fatal("> MULTIPART\n");
					        }
					        List<Range> partitionBarrel = Helpers.getPartitionBarrel(callbackData.getPartitionDims(), callbackData.getLength());					        
							while (true) {
								List<Range> rBarrel = Helpers.getRangeBarrel (rangeBarrel, partitionBarrel);
								if (rBarrel.size() == 0) break;					
								for (Range r : rBarrel) {
									if (r.partition != currentPartition) {
										callbackData.setRangePartition(r.partition);
										
										LOGGER.info (callbackData.getPath() + " > partition: " + r.partition);
										
										currentPartition = r.partition;
										try {
											new ServletImplementation ().run(callbackFunction, callbackData);
										} catch (Exception e) {
											break;
										} 										
									}

									LOGGER.info (callbackData.getPath() + " > send: " + r.start + "-" + r.end + "/" + r.length);						
									outputStream.write(callbackData.getMedia(), (int) r.start, (int) (r.end - r.start + 1));									
								}
								
							}											
							outputStream.flush();
							outputStream.close();	
						}
					}
				} else {
					OutputStream outputStream = res.getOutputStream();
					res.reset();
					res.setBufferSize(Constants.defaultBufferSize);	
					res.setContentType(TEXTPLAIN + "; charset=" + Constants.encoding);
										
					byte bos [] = callbackData.getOutput().getBytes(Constants.encoding);
                    if (callbackData.isEncodingGzip()) {
                        res.setHeader(CONTENTENCODING, GZIP);
                        outputStream = new GZIPOutputStream(outputStream, Constants.defaultBufferSize);
                    } else {
                        res.setHeader(CONTENTLENGTH, String.valueOf(bos.length));
                    }
					outputStream.write(bos);
					outputStream.flush();
					outputStream.close();													
				}
			} catch (IOException e) {
								
				StringWriter sw = new StringWriter();
			    PrintWriter pw = new PrintWriter(sw);
			    e.printStackTrace(pw);		
			    String error = sw.toString();
			    
			    LOGGER.info(repliedToCommand);	
			    LOGGER.fatal(error);		

			}	
			callbackData.close ();
		}
	}
	

	
}
