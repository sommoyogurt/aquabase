package com.tomting.aquabase.server.servlet;

import java.util.List;

import com.google.gson.Gson;
import com.tomting.aquabase.common.helpers.nio.WorkerPool;
import com.tomting.aquabase.server.orion.NioConnectionFactory;
import com.tomting.aquabase.server.orion.NioServiceTransport;
import com.tomting.aquabase.server.servlet.ServletHandler.HttpMethod;
import com.tomting.orion.ThrfSrvc;
import com.tomting.orion.ThrfSrvr;

public class ServletHandlerData {
	private String input;
	private String output = "";	
	private String path;
	private String callback;		
	private String print;
	private String filename;
	private byte[] media;		
	private String mediaContentType;
	private String eTag;
	private long lastModified;
	private long startTime;
	private long length;
	private int rangePartition;
	private boolean encodingGzip;	
	private List<Integer> partitionDims;
	private HttpMethod httpMethod;	
	private WorkerPool<ThrfSrvc, ThrfSrvr, NioServiceTransport> workerPool = NioConnectionFactory.get();
	private Gson gson = new Gson ();

	public ServletHandlerData () {
		setStartTime(System.currentTimeMillis());	
	}
			
	public void setOutput () {
	}
	
	public void close () {
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getCallback() {
		return callback;
	}

	public void setCallback(String callback) {
		this.callback = callback;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public byte[] getMedia() {
		return media;
	}

	public void setMedia(byte[] media) {
		this.media = media;
	}

	public String getMediaContentType() {
		return mediaContentType;
	}

	public void setMediaContentType(String mediaContentType) {
		this.mediaContentType = mediaContentType;
	}

	public String geteTag() {
		return eTag;
	}

	public void seteTag(String eTag) {
		this.eTag = eTag;
	}

	public long getLastModified() {
		return lastModified;
	}

	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getLength() {
		return length;
	}

	public void setLength(long length) {
		this.length = length;
	}

	public int getRangePartition() {
		return rangePartition;
	}

	public void setRangePartition(int rangePartition) {
		this.rangePartition = rangePartition;
	}

	public boolean isEncodingGzip() {
		return encodingGzip;
	}

	public void setEncodingGzip(boolean encodingGzip) {
		this.encodingGzip = encodingGzip;
	}

	public List<Integer> getPartitionDims() {
		return partitionDims;
	}

	public void setPartitionDims(List<Integer> partitionDims) {
		this.partitionDims = partitionDims;
	}

	public WorkerPool<ThrfSrvc, ThrfSrvr, NioServiceTransport> getWorkerPool() {
		return workerPool;
	}

	public Gson getGson() {
		return gson;
	}

	public String getPrint() {
		return print != null ? print : "";
	}

	public void setPrint(String print) {
		this.print = print;
	}

	public HttpMethod getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(HttpMethod httpMethod) {
		this.httpMethod = httpMethod;
	}
		
}	