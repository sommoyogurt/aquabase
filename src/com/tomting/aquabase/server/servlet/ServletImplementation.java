package com.tomting.aquabase.server.servlet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.tomting.aquabase.ThriftKeyValuePair;
import com.tomting.aquabase.ThriftReadResult;
import com.tomting.aquabase.ThriftWriteResult;
import com.tomting.aquabase.client.persistence.Reference;
import com.tomting.aquabase.client.persistence.ReferenceTree;
import com.tomting.aquabase.common.helpers.SimpleSerializer;
import com.tomting.aquabase.server.orion.NioServiceTransport;
import com.tomting.aquabase.server.servlet.ServletHandlerData;
import com.tomting.aquabase.server.thrift.GetValueCompletionListener;
import com.tomting.aquabase.server.thrift.SetValueCompletionListener;

public class ServletImplementation {
	private final static String HTTPPREFIX = "https:/";
	private HashMap<Integer,ServletHandlerListener> handlers;
	public static final String restJsonCommand = ".json";	
	public static final String restPrettyCommand = "pretty";
	
	
	public enum CallbackValue {
		GETVALUE;
	}
	
	public ServletImplementation () {
		handlers = new HashMap<Integer,ServletHandlerListener> ();
		
		handlers.put(CallbackValue.GETVALUE.ordinal(), new ServletHandlerListener() {			
			public void actionPerformed(ServletHandlerData data) throws Exception {getValue (data);}
		});	
	}
	
	public void run (CallbackValue callback, ServletHandlerData data) throws Exception {
		
		handlers.get (callback.ordinal()).actionPerformed (data);
	}	
	
	/**
	 * Json command (REST)
	 * @param reference
	 * @return
	 */
	private static boolean restJson (String address) {
		return address.endsWith(restJsonCommand);
	}
	
	/**
	 * Json net path (REST)
	 * @return
	 */
	public static String netPath (String address) {
		return address.replaceAll(restJsonCommand, "");
	}
	
	/**
	 * Execute Get command
	 * @param reference
	 * @param data
	 * @return
	 * @throws InterruptedException
	 */
	private static ThriftReadResult restGetCommand (Reference reference, ServletHandlerData data) throws InterruptedException {
		NioServiceTransport transport;
		ThriftReadResult readResult = new ThriftReadResult ();	
		transport = 
				new NioServiceTransport (	reference.getRoot().toUpperCase(), 
											reference.getName(), NioServiceTransport.Action.READ, 
											new GetValueCompletionListener (readResult));		
		data.getWorkerPool().addJob(transport);
		transport.syncWaitLock();
		return readResult;
	}
	
	/**
	 * Legacy Aquabase REST API
	 * @param data
	 */
	private static void getValue (ServletHandlerData data) {
		
		try {						
			String address = HTTPPREFIX + data.getPath ();
			Reference reference = new Reference (netPath (address));
			String name = reference.getName();
			String namespace = reference.getRoot().toUpperCase();
			NioServiceTransport transport;
			
			ReferenceTree tree;
			ThriftReadResult readResult;
			ArrayList<NioServiceTransport> transportPool;
			switch (data.getHttpMethod()) {
				case GET:	
					readResult = restGetCommand (reference, data);
					if (readResult.result) {
						if (restJson (address)) {						
							ReferenceTree treeModel = new ReferenceTree ();
							for (ThriftKeyValuePair k : readResult.keyValuePair) 
								treeModel.addSubReference(k.key.substring(name.length() + 1), SimpleSerializer.deserialize(k.value).toString());
							data.setOutput (treeModel.toJson(data.getPrint().equals(restPrettyCommand)));
						}	else {				
							String value = readResult.keyValuePair.get(0).value;
							data.setOutput(SimpleSerializer.deserialize(value).toString());
						}
					} 				
					break;
				case PUT:
				case DELETE:
					readResult = restGetCommand (reference, data);
					transportPool = new ArrayList <NioServiceTransport> ();
					if (readResult.result) {
						for (ThriftKeyValuePair k : readResult.keyValuePair) {
							ThriftWriteResult writeResult = new ThriftWriteResult ().setResult(false);
							transport = new NioServiceTransport (	  
									namespace, 
									k.key,
									null, 
									NioServiceTransport.Action.DELETE,
									new SetValueCompletionListener (writeResult));	
							transportPool.add(transport);
							data.getWorkerPool().addJob(transport);		
						}
						for ( NioServiceTransport t : transportPool) t.syncWaitLock();	
					}
					break;					
				default:
					break;
			}
		switch (data.getHttpMethod()) {
			case PATCH:
			case PUT:
				transportPool = new ArrayList <NioServiceTransport> ();
				tree = new ReferenceTree ();	
				tree.fromJson(data.getInput());	
								
				for (Entry<Reference, Object> s : tree.toReference (reference).entrySet()) {		
					ThriftWriteResult writeResult = new ThriftWriteResult ().setResult(false);	
					transport = new NioServiceTransport (	  
							namespace, 
							s.getKey().getComparableKey(),
							SimpleSerializer.serialize((Serializable) s.getValue()), 
							NioServiceTransport.Action.WRITE,
							new SetValueCompletionListener (writeResult));
					
					transportPool.add(transport);
					data.getWorkerPool().addJob(transport);						
				}	
				for ( NioServiceTransport t : transportPool) t.syncWaitLock();	
				data.setOutput(data.getInput());
				break;
			default:
				break;	
			}
				
		} catch (Exception e) {}
	}
}
