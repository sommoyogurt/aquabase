namespace java com.tomting.aquabase


//namespace, common
	struct ThriftUserNamespace {
		1: required string userNamespace;	  		
	}  
		
//core
	struct ThriftWriteResult {
		1: bool result;
	}
	
	struct ThriftKeyValuePair {
		1: required string key;
		2: string value;
	}

	struct ThriftWrite {
		1: ThriftUserNamespace userNamespace;
		2: ThriftKeyValuePair keyValuePair;
	}
	
	struct ThriftReadResult {
		1: bool result;	
		2: list<ThriftKeyValuePair> keyValuePair;
	}	

//service
	service ThriftAquabase {
		bool ping ();
				
		list<ThriftWriteResult> setValue (1: list<ThriftWrite> bulkWrite);		
	}


